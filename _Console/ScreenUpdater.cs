﻿namespace TheGallowsSurvival._Console
{
    internal class ScreenUpdater
    {
        private FrameMaker _frameProvider;

        public ScreenUpdater(FrameMaker frameProvider)
        {
            _frameProvider = frameProvider;
        }

        public void ShowFrame()
        {
            string frame;

            if (_frameProvider != null)
            {
                frame = _frameProvider.GetFrame();
            }
            else
            {
                frame = "[ERROR] NO FRAME PROVIDER";
            }

            Console.Clear();
            Console.Out.Write(frame);
        }
    }
}