﻿using System.Media;
using System.Reflection;
using TheGallowsSurvival.GameScene;

namespace TheGallowsSurvival
{
    internal static class GameSoundPlayer
    {


        private static SoundPlayer _buttonSound;
        private static SoundPlayer _clickingButtonSound;
        private static SoundPlayer _damageSound;
        private static SoundPlayer _regenarationSound;
        private static SoundPlayer _gameOverSound;

        public static void LoadSounds()
        {
            var appPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            var buttonSoundPath = Path.Combine(appPath, @"Sounds\sfx_coin_double7.wav");
            _buttonSound = new SoundPlayer(buttonSoundPath);
            var clickingButtonSoundPath = Path.Combine(appPath, @"Sounds\sfx_menu_move3.wav");
            _clickingButtonSound = new SoundPlayer(clickingButtonSoundPath);
            var damageSoundPath = Path.Combine(appPath, @"Sounds\sfx_sounds_impact12.wav");
            _damageSound = new SoundPlayer(damageSoundPath);
            var regenerationSoundPath = Path.Combine(appPath, @"Sounds\sfx_sounds_interaction11.wav");
            _regenarationSound = new SoundPlayer(regenerationSoundPath);
            var gameOverSound = Path.Combine(appPath, @"Sounds\sfx_sound_shutdown2.wav");
            _gameOverSound = new SoundPlayer(gameOverSound);

            _buttonSound.Load();
            _clickingButtonSound.Load();
            _damageSound.Load();
            _regenarationSound.Load();
            _gameOverSound.Load();

        }


        private static void MuteSound()
        {
            _buttonSound.Play();
            _buttonSound.Stop();
        }

        public static void PlayButtonSound()
        {
            if (GameWorld.IsSoundEnabled)
            {
                _buttonSound.Play();
            }
            
        }
        public static void PlayButtonClickSound()
        {
            if (GameWorld.IsSoundEnabled)
            {
                _clickingButtonSound.Play();
            }
        }

        public static void PlayDamageSound()
        {
            if (GameWorld.IsSoundEnabled)
            {
                _damageSound.Play();
            }
        }

        public static void PlayRegenarationSound()
        {
            if (GameWorld.IsSoundEnabled)
            {
                _regenarationSound.Play();
            }
        }
        
        public static void PlayGameOverSound()
        {
            if (GameWorld.IsSoundEnabled)
            {
                _gameOverSound.Play();
            }
        }
    }
}