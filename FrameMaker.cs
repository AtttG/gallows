﻿using Pastel;
using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival
{
    internal class FrameMaker
    {
        private List<List<Pixel>> _defaultLayout = new(GameWorld.Screen.HEIGHT);
        private List<List<Pixel>> _layout;
        private List<List<Pixel>> _previousLayout;

        public FrameMaker()
        {
            SetDefaultLayout();
            _layout = new(_defaultLayout);
        }

        private void SetDefaultLayout()
        {
            for (int y = 0; y < GameWorld.Screen.HEIGHT; y++)
            {
                _defaultLayout.Add(new(GameWorld.Screen.WIDTH));

                for (int x = 0; x < GameWorld.Screen.WIDTH; x++)
                {
                    _defaultLayout[y].Add(new Pixel(' '));
                }
            }
        }

        private List<List<Pixel>> CopyLayout(List<List<Pixel>> layout)
        {
            List<List<Pixel>> copy = new(layout);

            for (int i = 0; i < _layout.Count; i++)
            {
                copy[i] = new(_defaultLayout[i]);
            }

            return copy;
        }

        private bool IsPointOnScreen(RepresantationPoint point)
        {
            return (point.Position.X >= GameWorld.Screen.CameraPoint.X
                    && point.Position.Y >= GameWorld.Screen.CameraPoint.Y
                    && point.Position.X < GameWorld.Screen.CameraPoint.X + GameWorld.Screen.CAMERA_WIDTH
                    && point.Position.Y < GameWorld.Screen.CameraPoint.Y + GameWorld.Screen.CAMERA_HEIGHT);
        }

        public string GetFrame()
        {
            _previousLayout = CopyLayout(_layout);
            _layout = CopyLayout(_defaultLayout);

            var gameObjectsLayers = GameWorld.CurrentGameScene.GetGameObjects();
            var staticObjectsLayers = GameWorld.CurrentGameScene.GetStaticObjects();

            // Объекты с позицией привязаной к игровому миру
            foreach (var layer in gameObjectsLayers)
            {
                foreach (var obj in layer)
                {
                    foreach (var point in obj.Represantation)
                    {
                        if (IsPointOnScreen(point))
                        {
                            _layout[GameWorld.Screen.OFFSET.Y + (point.Position.Y - GameWorld.Screen.CameraPoint.Y)][GameWorld.Screen.OFFSET.X + (point.Position.X - GameWorld.Screen.CameraPoint.X)] = point.Pixel;
                        }
                    }
                }
            }

            // Объекты с позицией привязаной к экрану
            foreach (var layer in staticObjectsLayers)
            {
                foreach (var obj in layer)
                {
                    foreach (var point in obj.Represantation)
                    {
                        _layout[point.Position.Y][point.Position.X] = point.Pixel;
                    }
                }
            }

            //Point cameraPoint = GameWorld.Screen.CameraPoint;
            string frame = string.Empty;
            string subString = string.Empty;
            Color lastColor = _layout[0][0].Foreground;

            Color difficultyColor = Color.White;

            if (GameWorld.CurrentGameScene != GameWorld.MainMenu || GameWorld.MainMenu.SelectingDifficulty)
            {
                switch (GameWorld.MainMenu.SelectedDifficulty)
                {
                    case GameScenes.GameDifficulty.Easy:
                        difficultyColor = Color.FromArgb(255, 100, 175, 100);
                        break;

                    case GameScenes.GameDifficulty.Medium:
                        difficultyColor = Color.FromArgb(255, 200, 200, 100);
                        break;

                    case GameScenes.GameDifficulty.Hard:
                        difficultyColor = Color.FromArgb(255, 200, 100, 100);
                        break;
                }
            }

            for (int y = 0; y < GameWorld.Screen.HEIGHT; y++)
            {
                for (int x = 0; x < GameWorld.Screen.WIDTH; x++)
                {
                    subString += _layout[y][x].Value;
                    lastColor = _layout[y][x].Foreground;

                    if (x == (GameWorld.Screen.WIDTH - 1))
                    {
                        if ((y != GameWorld.Screen.HEIGHT - 1))
                        {
                            subString += '\n';
                            if ((_layout[y][x].Foreground != _layout[y + 1][0].Foreground))
                            {
                                if (y == 0)
                                {
                                    frame += subString.Pastel(lastColor).PastelBg(difficultyColor);
                                }
                                else
                                {
                                    frame += subString.Pastel(lastColor);
                                }
                                //frame += subString.Pastel(lastColor);
                                subString = string.Empty;
                            }
                        }
                        else
                        {
                            if (y == 0)
                            {
                                frame += subString.Pastel(lastColor).PastelBg(difficultyColor);
                            }
                            else
                            {
                                frame += subString.Pastel(lastColor);
                            }
                            subString = string.Empty;
                        }
                    }
                    else if ((_layout[y][x + 1].Foreground != _layout[y][x].Foreground))
                    {
                        if (y == 0)
                        {
                            frame += subString.Pastel(lastColor).PastelBg(difficultyColor);
                        }
                        else
                        {
                            frame += subString.Pastel(lastColor);
                        }
                        subString = string.Empty;
                    }
                }
            }

            return frame;
        }
    }
}