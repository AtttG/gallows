﻿namespace TheGallowsSurvival.GameObjects
{
    internal interface IDynamicGameObject
    {
        public void Update();

        public bool IsBusy();
    }
}