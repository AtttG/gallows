﻿using System.Drawing;
using TheGallowsSurvival.GameObjects.Bodies;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects
{
    internal class GameObject : IRenderableObject
    {
        public GameObject(List<ObjectBody> body)
        {
            _body = body;
        }

        public virtual void Move(Point position)
        {
            Move(position.X, position.Y);
        }

        public virtual void Move(int x, int y)
        {
            foreach (var bodyVersion in Body)
            {
                bodyVersion.Move(x, y);
            }
        }

        public virtual void MoveTo(Point position)
        {
            MoveTo(position.X, position.Y);
        }

        public virtual void MoveTo(int x, int y)
        {
            foreach (var bodyVersion in Body)
            {
                bodyVersion.MoveTo(x, y);
            }
        }

        private List<ObjectBody> _body;

        public List<ObjectBody> Body
        {
            get
            {
                return _body;
            }
            private set { _body = value; }
        }

        public virtual List<RepresantationPoint> Represantation
        {
            get { return Body[0].GetBody(); }
        }
    }
}