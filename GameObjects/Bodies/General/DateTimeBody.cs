﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.General
{
    internal class DateTimeBody : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            var time = StringToPoints(DateTime.Now.ToString(), Color.Black);
            body.AddRange(time);
            return body;
        }
    }
}