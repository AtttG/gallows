﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.General
{
    internal class SoundLogoBody : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            var color = Color.FromArgb(255, 25, 25, 25);
            //var color = Color.Black;

            body.Add(new RepresantationPoint(new Point(0, 0), new Pixel('<', color)));
            body.Add(new RepresantationPoint(new Point(1, 0), new Pixel('|', color)));
            if (GameWorld.IsSoundEnabled)
            {
                body.Add(new RepresantationPoint(new Point(2, 0), new Pixel(')', color)));
            }
            else
            {
                body.Add(new RepresantationPoint(new Point(2, 0), new Pixel('X', Color.Red)));
            }
            return body;
        }
    }
}