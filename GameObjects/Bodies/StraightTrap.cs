﻿namespace TheGallowsSurvival.GameObjects.Bodies
{
    internal class StraightTrap : GameObject, IDynamicGameObject
    {
        public enum TrapDirection
        { UP, RIGHT, DOWN, LEFT }

        private int _moveCounter = 0;
        private int _moveLimit;
        private TrapDirection _direction;
        private long _lastTime;
        private int _speed;

        public StraightTrap(List<ObjectBody> body, int size, TrapDirection direction, int speed) : base(body)
        {
            _moveLimit = size;
            _direction = direction;
            _speed = speed;
        }

        public bool IsBusy()
        {
            return false;
        }

        public void Update()
        {
            var currentTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            if ((currentTime - _lastTime) > _speed)
            {
                switch (_direction)
                {
                    case TrapDirection.UP:
                        this.Move(0, -1);
                        break;

                    case TrapDirection.RIGHT:
                        this.Move(1, 0);
                        break;

                    case TrapDirection.DOWN:
                        this.Move(0, 1);
                        break;

                    case TrapDirection.LEFT:
                        this.Move(-1, 0);
                        break;
                }
                _moveCounter++;

                _lastTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

                if (_moveCounter >= _moveLimit)
                {
                    switch (_direction)
                    {
                        case TrapDirection.UP:
                            this.Move(0, _moveLimit);
                            break;

                        case TrapDirection.RIGHT:
                            this.Move(-_moveLimit, 0);
                            break;

                        case TrapDirection.DOWN:
                            this.Move(0, _moveLimit);
                            break;

                        case TrapDirection.LEFT:
                            this.Move(_moveLimit, 0);
                            break;
                    }

                    _moveCounter = 0;

                    //Body.Clear();
                }
            }
        }
    }
}