﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.CharacterCreation
{
    internal class CharacterCreationPlayButtonBodyUnselected : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.FromArgb(255, 250, 250, 250);

            List<RepresantationPoint> body = new();

            var borders = ObjectBody.StringToPoints("┌─────────────────────────────────────┐\n" +
                                                    "│                                     │\n" +
                                                    "│                                     │\n" +
                                                    "│                                     │\n" +
                                                    "│                                     │\n" +
                                                    "└─────────────────────────────────────┘", color);

            var initialWord = "Начать";
            var word = ObjectBody.StringToPoints(initialWord, color: color, translation: new Point((38 - initialWord.Length) / 2, 2));

            body.AddRange(borders);
            body.AddRange(word);
            return body;
        }
    }
}