﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.CharacterCreation
{
    internal class CharacterCreationPlayButtonSelected : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var color = ColorController.GetRgbColor();

            List<RepresantationPoint> body = new();

            var borders = ObjectBody.StringToPoints("┌─────────────────────────────────────┐\n" +
                                                    "│                                     │\n" +
                                                    "│                                     │\n" +
                                                    "│                                     │\n" +
                                                    "│                                     │\n" +
                                                    "╘═════════════════════════════════════╛", color);

            string initialWord;

            if (GameWorld.CharecterCreation.ReadyToStart)
            {
                initialWord = "Начать";
            }
            else
            {
                initialWord = " Создайте хотя бы одного героя";
                color = Color.FromArgb(255, 255, 100, 100);
            }

            var word = ObjectBody.StringToPoints(initialWord, color: color, translation: new Point((38 - initialWord.Length) / 2, 2));

            body.AddRange(borders);
            body.AddRange(word);
            return body;
        }
    }
}