﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.CharacterCreation
{
    internal class CharacterButtonBodyAnimationOccupied : DynamicBodyObject
    {
        private CharacterCreationScene.ButtonType _type;

        public CharacterButtonBodyAnimationOccupied(CharacterCreationScene.ButtonType selfType)
        {
            _type = selfType;
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            var myCharacter = GameWorld.CharecterCreation.GetGameCharacter(_type);
            var color = ((GameWorld.CharecterCreation.CurrentButton == _type)) ? ColorController.GetRgbColor() : Color.White;
            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
                "┌─────────────────────────────────────┐" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "├──┐                               ┌──┤" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "├──┘                               └──┤" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│ ИМЯ:                                │" + '\n' +
                "│ КЛАСС:                              │" + '\n' +
                "└─────────────────────────────────────┘", color
                ));

            if (myCharacter != null && !(GameWorld.CharecterCreation.CurrentButton == _type && GameWorld.CharecterCreation.CurrentCreationStage != CharacterCreationScene.CreationStage.None))
            {
                body.AddRange(ObjectBody.StringToPoints("┬     ┬\n└ГОТОВ┘", color, new Point(16, 0)));
            }

            if (GameWorld.CharecterCreation.CurrentButton == _type)
            {
                switch (GameWorld.CharecterCreation.CurrentCreationStage)
                {
                    case CharacterCreationScene.CreationStage.Class:
                        {
                            body.AddRange((ObjectBody.StringToPoints(
                            "[ Выберите класс ]", Color.White, new Point(10, 3))));

                            var currentCharacterBody = CharacterCreationScene.CharacterBodyByType(GameWorld.CharecterCreation.ChosenCharacterType);
                            currentCharacterBody.MoveTo(14, 7);
                            body.AddRange(currentCharacterBody.GetBody());

                            var typeName = CharacterCreationScene.CharacterTypeToRus(GameWorld.CharecterCreation.ChosenCharacterType);
                            body.AddRange((ObjectBody.StringToPoints(
                            typeName, Color.White, new Point((39 - typeName.Length) / 2, 13)
                            )));
                            body.AddRange(new List<RepresantationPoint>
                            {
                                new RepresantationPoint(new Point(8, 13), new Pixel('<', Color.White)),
                                new RepresantationPoint(new Point(30, 13), new Pixel('>', Color.White))
                            });
                        }
                        break;

                    case CharacterCreationScene.CreationStage.Name:
                        {
                            body.AddRange((ObjectBody.StringToPoints(
                                "[ Введите имя ]", Color.White, new Point(12, 3))));

                            var currentCharacterBody = CharacterCreationScene.CharacterBodyByType(GameWorld.CharecterCreation.ChosenCharacterType);
                            currentCharacterBody.MoveTo(14, 8);
                            body.AddRange(currentCharacterBody.GetBody());

                            body.AddRange((ObjectBody.StringToPoints(
                             GameWorld.CharecterCreation.CharacterName, Color.White, new Point(7, 18)
                             )));
                            body.AddRange((ObjectBody.StringToPoints(
                            CharacterCreationScene.CharacterTypeToRus(GameWorld.CharecterCreation.ChosenCharacterType), Color.White, new Point(9, 19)
                            )));
                        }
                        break;

                    case CharacterCreationScene.CreationStage.None:
                        {
                            if (myCharacter != null && GameWorld.CharecterCreation.CurrentCreationStage == CharacterCreationScene.CreationStage.None)
                            {
                                var currentCharacterBody = CharacterCreationScene.CharacterBodyByType(myCharacter.Type, myCharacter.CharacterColor);
                                currentCharacterBody.MoveTo(14, 8);
                                body.AddRange(currentCharacterBody.GetBody());

                                body.AddRange((ObjectBody.StringToPoints(
                                    myCharacter.Name, Color.White, new Point(7, 18)
                                    )));
                                body.AddRange((ObjectBody.StringToPoints(
                                    CharacterCreationScene.CharacterTypeToRus(myCharacter.Type), Color.White, new Point(9, 19)
                                    )));
                            }
                        }
                        break;
                }
            }
            else
            {
                if (myCharacter != null)
                {
                    var currentCharacterBody = CharacterCreationScene.CharacterBodyByType(myCharacter.Type, myCharacter.CharacterColor);
                    currentCharacterBody.MoveTo(14, 8);

                    body.AddRange(currentCharacterBody.GetBody());

                    body.AddRange((ObjectBody.StringToPoints(
                        myCharacter.Name, Color.White, new Point(7, 18)
                        )));
                    body.AddRange((ObjectBody.StringToPoints(
                        CharacterCreationScene.CharacterTypeToRus(myCharacter.Type), Color.White, new Point(9, 19)
                        )));
                }
            }

            return body;
        }
    }
}