﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.CharacterCreation
{
    internal class CharacterButtonBodyAnimationEmpty : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.Gray;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
                "┌─────────────────────────────────────┐" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "├──┐                               ┌──┤" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "├──┘                               └──┤" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "└─────────────────────────────────────┘", color
                ));
            return body;
        }
    }

    internal class CharacterButtonBodyAnimationEmpty2 : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.Gray;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
                "┌─────────────────────────────────────┐" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "├──┐                               ┌──┤" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │           С                   │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│──│                               │──│" + '\n' +
                "├──┘                               └──┤" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "└─────────────────────────────────────┘", color
                ));
            return body;
        }
    }

    internal class CharacterButtonBodyAnimationEmpty3 : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.Gray;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
                "┌─────────────────────────────────────┐" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "├──┐                               ┌──┤" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │           СО                  │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "├──┘                               └──┤" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "└─────────────────────────────────────┘", color
                ));
            return body;
        }
    }

    internal class CharacterButtonBodyAnimationEmpty4 : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.Gray;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
                "┌─────────────────────────────────────┐" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "├──┐                               ┌──┤" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │           СОЗ                 │  │" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "├──┘                               └──┤" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "└─────────────────────────────────────┘", color
                ));
            return body;
        }
    }

    internal class CharacterButtonBodyAnimationEmpty5 : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.Gray;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
                "┌─────────────────────────────────────┐" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "├──┐                               ┌──┤" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│──│           СОЗД                │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "├──┘                               └──┤" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "└─────────────────────────────────────┘", color
                ));
            return body;
        }
    }

    internal class CharacterButtonBodyAnimationEmpty6 : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.Gray;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
                "┌─────────────────────────────────────┐" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "├──┐                               ┌──┤" + '\n' +
                "│  │                               │  │" + '\n' +
                "│  │                               │  │" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│           СОЗДА               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "├──┘                               └──┤" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "└─────────────────────────────────────┘", color
                ));
            return body;
        }
    }

    internal class CharacterButtonBodyAnimationEmpty7 : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.Gray;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
                "┌─────────────────────────────────────┐" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "├──┐                               ┌──┤" + '\n' +
                "│  │                               │  │" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│           СОЗДАТ              │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "├──┘                               └──┤" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "└─────────────────────────────────────┘", color
                ));
            return body;
        }
    }

    internal class CharacterButtonBodyAnimationEmpty8 : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.Gray;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
                "┌─────────────────────────────────────┐" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "├──┐                               ┌──┤" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│           СОЗДАТЬ             │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "├──┘                               └──┤" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "└─────────────────────────────────────┘", color
                ));
            return body;
        }
    }

    internal class CharacterButtonBodyAnimationEmpty9 : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            //var color = Color.FromArgb(255, 100, 250, 100);
            var color = ColorController.GetRgbColor();
            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
                "┌─────────────────────────────────────┐" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "├──┐                               ┌──┤" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│           СОЗДАТЬ             │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "│──│                               │──│" + '\n' +
                "├──┘                               └──┤" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "│                                     │" + '\n' +
                "└─────────────────────────────────────┘", color
                ));
            return body;
        }
    }
}