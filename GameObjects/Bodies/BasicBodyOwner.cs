﻿using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies
{
    internal class BasicBodyOwner : GameObject
    {
        public int State = 0;

        public BasicBodyOwner(List<ObjectBody> body) : base(body)
        {
        }

        public override List<RepresantationPoint> Represantation
        {
            get
            {
                return Body[State].GetBody();
            }
        }
    }
}