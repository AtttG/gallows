﻿using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies
{
    internal class CharacterButtonBodyOwner : GameObject, IDynamicGameObject
    {
        public override void Move(int x, int y)
        {
            base.Move(x, y);
            foreach (var bodyVersion in _bodyOccupied)
            {
                bodyVersion.Move(x, y);
            }
        }

        public override void MoveTo(int x, int y)
        {
            base.MoveTo(x, y);
            foreach (var bodyVersion in _bodyOccupied)
            {
                bodyVersion.MoveTo(x, y);
            }
        }

        public int State
        {
            get { return _state; }
            set
            {
                if (value > Body.Count - 1) { value = Body.Count - 1; }
                else if (value < 0) { value = 0; }
                _state = value;
            }
        }

        private CharacterCreationScene _scene;
        private CharacterCreationScene.ButtonType _type;
        private long _lastUpdate = 0;

        private List<ObjectBody> _bodyOccupied;

        private int _state = 0;

        public CharacterButtonBodyOwner(List<ObjectBody> bodyEmpty, List<ObjectBody> bodyOccupied, CharacterCreationScene scene, CharacterCreationScene.ButtonType selfType) : base(bodyEmpty)
        {
            _scene = scene;
            _type = selfType;
            _bodyOccupied = bodyOccupied;
        }

        public override List<RepresantationPoint> Represantation
        {
            get
            {
                if (_scene.GetGameCharacter(_type) != null || (_scene.CurrentButton == _type && _scene.CurrentCreationStage != CharacterCreationScene.CreationStage.None))
                {
                    return _bodyOccupied[0].GetBody();
                }
                return Body[State].GetBody();
            }
        }

        public void Update()
        {
            var currentTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            if ((currentTime - _lastUpdate) >= 75)
            {
                if (_scene.CurrentButton == _type)
                {
                    State++;
                }
                else
                {
                    State--;
                }
                _lastUpdate = currentTime;
            }
        }

        public bool IsBusy()
        {
            if (_scene.GetGameCharacter(_type) != null)
            {
                return false;
            }
            if (_scene.CurrentButton == _type && State == Body.Count - 1)
            {
                return false;
            }
            if (_scene.CurrentButton != _type && State == 0)
            {
                return false;
            }
            return true;
        }
    }
}