﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies
{
    internal abstract class ObjectBody
    {
        public static List<RepresantationPoint> StringToPoints(string inputString, Color? color = null, Point? translation = null)
        {
            if (!color.HasValue)
            {
                color = Color.White;
            }
            var body = new List<RepresantationPoint>();

            int y = 0;

            int x_position = -1;

            for (int k = 0; k < inputString.Length; k++)
            {
                x_position++;
                if (inputString[k] == '\n')
                {
                    y++;
                    x_position = -1;
                }
                else if (inputString[k] != ' ')
                {
                    Point position = new Point(x_position, y);
                    if (translation.HasValue)
                    {
                        position = new Point(position.X + translation.Value.X, position.Y + translation.Value.Y);
                    }

                    body.Add(new RepresantationPoint(position, new Pixel(inputString[k], color)));
                }
            }
            return body;
        }

        protected List<RepresantationPoint> Body;
        public Point TargetPosition = new(0, 0);

        public abstract List<RepresantationPoint> GetBody();

        public abstract List<RepresantationPoint> InitializeBody();

        public virtual void MoveTo(int x, int y)
        {
            TargetPosition = new Point(x, y);
        }

        public virtual void Move(int x, int y)
        {
            TargetPosition = new Point(TargetPosition.X + x, TargetPosition.Y + y);
        }
    }

    internal class DynamicBodyObject : ObjectBody
    {
        public override List<RepresantationPoint> GetBody()
        {
            Body = InitializeBody();

            foreach (var point in Body)
            {
                point.Position = new Point(point.Position.X + TargetPosition.X, point.Position.Y + TargetPosition.Y);
            }

            return Body;
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            throw new NotImplementedException();
        }
    }

    internal class StaticBodyObject : ObjectBody
    {
        public override List<RepresantationPoint> GetBody()
        {
            if (Body == null) { Body = InitializeBody(); }

            return Body;
        }

        public override void MoveTo(int x, int y)
        {
            Body = InitializeBody();
            TargetPosition = new Point(x, y);

            foreach (var point in Body)
            {
                point.Position = new Point(point.Position.X + x, point.Position.Y + y);
            }
        }

        public override void Move(int x, int y)
        {
            Body = InitializeBody();
            TargetPosition = new Point(TargetPosition.X + x, TargetPosition.Y + y);
            foreach (var point in Body)
            {
                point.Position = new Point(TargetPosition.X + point.Position.X, TargetPosition.Y + point.Position.Y);
            }
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            throw new NotImplementedException();
        }
    }
}