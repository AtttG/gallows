﻿using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.Enemies
{
    internal class OutlawBody : DynamicBodyObject
    {
        private System.Drawing.Color _selfColor;

        public OutlawBody(System.Drawing.Color color)
        {
            _selfColor = color;
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
            @"  |\|\" + '\n' +
            @"  /--|" + '\n' +
            @" /|__|\" + '\n' +
            @"  /  \", System.Drawing.Color.White
             ));

            body.AddRange(ObjectBody.StringToPoints(
            @"       ]  " + '\n' +
            @"   --  |  " + '\n' +
            @"       | " + '\n' +
            @"       |  ", System.Drawing.Color.Red
             ));

            body.AddRange(ObjectBody.StringToPoints(
            @" " + '\n' +
            @"   --", _selfColor
             ));

            return body;
        }
    }
}