﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.Characters
{
    internal class HackerBody : DynamicBodyObject
    {
        private Color _playerColor;

        public HackerBody(Color playerColor)
        {
            _playerColor = playerColor;
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.White;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
               @"   ┌┌┌┌   ", _playerColor
                ));
            body.AddRange(ObjectBody.StringToPoints(
               '\n' +
               @"  [    ]   " + '\n' +
               @"  [[__]]" + '\n' +
               @"   │  │    ", color
                ));

            var glasses = new List<RepresantationPoint>()
            {
                new RepresantationPoint(new Point(4, 1), new Pixel('═', Color.DarkGray)),
                new RepresantationPoint(new Point(5, 1), new Pixel('═', Color.DarkGray)),
                new RepresantationPoint(new Point(6, 1), new Pixel('═', Color.DarkGray)),
            };

            var currentTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            int currentTimeIndex = (int)(currentTime % (3 * 150)) / 150;

            glasses[currentTimeIndex].Pixel.Foreground = Color.Cyan;

            body.AddRange(new List<RepresantationPoint>()
            {
                new RepresantationPoint( new Point(3,1), new Pixel('-', Color.DarkGray)),
            });

            body.AddRange(glasses);

            return body;
        }
    }
}