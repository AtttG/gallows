﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.Characters
{
    internal class HealerBody : DynamicBodyObject
    {
        private Color _playerColor;

        public HealerBody(Color playerColor)
        {
            _playerColor = playerColor;
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.White;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
               '\n' +
               @"  (    ) " + '\n' +
               @"╤/ {__} \" + '\n' +
               @"│  }  {  ", color
                ));

            var index = (int)(DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() % (10 * 133)) / 133;

            var colorIndex = (int)(DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() % (200 * 5)) / 5;

            if (colorIndex >= 100)
            {
                colorIndex = 200 - colorIndex;
            }
            var ballColor = Color.FromArgb(255, colorIndex + 155, colorIndex + 155, colorIndex);
            var pathColor = Color.FromArgb(255, colorIndex / 2, (colorIndex + 155) / 2, colorIndex / 2);

            if (index == 0)
            {
                body.AddRange(ObjectBody.StringToPoints(
                @"  *" + '\n' + '\n' +
                @"       *", ballColor
                 ));

                body.AddRange(ObjectBody.StringToPoints(
                @"    +", pathColor
                ));
            }
            else if (index == 1)
            {
                body.AddRange(ObjectBody.StringToPoints(
                '\n' +
                @"*        *", ballColor
                 ));

                body.AddRange(ObjectBody.StringToPoints(
@"  +" + '\n' + '\n' +
@"       +", pathColor
 ));
            }
            else if (index == 2)
            {
                body.AddRange(ObjectBody.StringToPoints(
                @"       *" + '\n' + '\n' +
                @"  *", ballColor
                 ));

                body.AddRange(ObjectBody.StringToPoints(
'\n' +
@"+        +", pathColor
 ));
            }
            else if (index == 3)
            {
                body.AddRange(ObjectBody.StringToPoints(
                @"     *", ballColor
                 ));

                body.AddRange(ObjectBody.StringToPoints(
@"       +" + '\n' + '\n' +
@"  +", pathColor
 ));
            }
            else if (index == 4)
            {
                body.AddRange(ObjectBody.StringToPoints(
                @"    *", ballColor
                 ));

                body.AddRange(ObjectBody.StringToPoints(
@"     +", pathColor
 ));
            }
            else if (index == 5)
            {
                body.AddRange(ObjectBody.StringToPoints(
                    @"  *" + '\n' + '\n' +
                    @"       *", ballColor
                     ));

                body.AddRange(ObjectBody.StringToPoints(
@"    +", pathColor
 ));
            }
            else if (index == 6)
            {
                body.AddRange(ObjectBody.StringToPoints(
                    '\n' +
                    @"*        *", ballColor
                    ));

                body.AddRange(ObjectBody.StringToPoints(
    @"  +" + '\n' + '\n' +
    @"       +", pathColor
     ));
            }
            else if (index == 7)
            {
                body.AddRange(ObjectBody.StringToPoints(
                    @"       *" + "\n\n" +
                    @"  *", ballColor
                    ));

                body.AddRange(ObjectBody.StringToPoints(
    '\n' +
    @"+        +", pathColor
    ));
            }
            else if (index == 8)
            {
                body.AddRange(ObjectBody.StringToPoints(
                    @"     *", ballColor
                    ));

                body.AddRange(ObjectBody.StringToPoints(
    @"       +" + "\n\n" +
    @"  +", pathColor
    ));
            }
            else if (index == 9)
            {
                body.AddRange(ObjectBody.StringToPoints(
                    @"    *", ballColor
                    ));

                body.AddRange(ObjectBody.StringToPoints(
    @"     +", pathColor
    ));
            }

            body.AddRange(ObjectBody.StringToPoints(
               '\n' + @"    ─ -", _playerColor
                ));
            return body;
        }
    }
}