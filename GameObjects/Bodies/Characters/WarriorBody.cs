﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.Characters
{
    internal class WarriorBody : DynamicBodyObject
    {
        private Color _playerColor;

        public WarriorBody(Color playerColor)
        {
            _playerColor = playerColor;
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            var color = Color.White;

            List<RepresantationPoint> body = new();

            body.AddRange(ObjectBody.StringToPoints(
               @"         |  " + '\n' +
               @"  (    ) │  " + '\n' +
               @"  [|__| \│  " + '\n' +
               @"   |  |  |  ", color
                ));

            var index = (int)(DateTimeOffset.UtcNow.ToUnixTimeMilliseconds() % (2 * 100)) / 100;

            if (index == 0)
            {
                body.AddRange(ObjectBody.StringToPoints(
                @"        { ) " + "\n\n\n" +
                @"        ( }  ", Color.FromArgb(255, 200, 20, 20)
                 ));
            }
            else
            {
                body.AddRange(ObjectBody.StringToPoints(
                @"        ( } " + "\n\n\n" +
                @"        { )  ", Color.FromArgb(255, 255, 20, 20)
                 ));
            }

            body.AddRange(ObjectBody.StringToPoints(
               '\n' + @"    ═ ═", _playerColor
                ));
            return body;
        }
    }
}