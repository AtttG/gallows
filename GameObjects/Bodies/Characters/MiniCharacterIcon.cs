﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.Characters
{
    internal class MiniCharacterIcon : StaticBodyObject
    {
        private Color _leftCharacter = Color.White;
        private Color _midCharacter = Color.White;
        private Color _rightCharacter = Color.White;

        public MiniCharacterIcon()
        {
            if (GameWorld.CharecterCreation.LeftCharacter != null)
            {
                _leftCharacter = GameWorld.CharecterCreation.LeftCharacter.CharacterColor;
            }
            if (GameWorld.CharecterCreation.MidCharacter != null)
            {
                _midCharacter = GameWorld.CharecterCreation.MidCharacter.CharacterColor;
            }
            if (GameWorld.CharecterCreation.RightCharacter != null)
            {
                _rightCharacter = GameWorld.CharecterCreation.RightCharacter.CharacterColor;
            }
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            body.Add(new RepresantationPoint(new Point(0, 0), new Pixel('[', _leftCharacter)));
            body.Add(new RepresantationPoint(new Point(1, 0), new Pixel('V', _midCharacter)));
            body.Add(new RepresantationPoint(new Point(2, 0), new Pixel(']', _rightCharacter)));

            return body;
        }
    }
}