﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.MainMenu
{
    internal class MenuExitButtonBodyUnselected : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            var color = Color.Gray;

            List<RepresantationPoint> leftCol = new()
                {
                    new RepresantationPoint(new Point(1,0), new Pixel('┌', color)),
                    new RepresantationPoint(new Point(1,1), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(1,2), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(1,3), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(1,4), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(1,5), new Pixel('└', color))
                };

            var initialWord = "Выход";
            var word = StringToPoints(initialWord, color: color, translation: new Point((40 - initialWord.Length) / 2, 2));

            List<RepresantationPoint> rightCol = new()
                {
                    new RepresantationPoint(new Point(39,0), new Pixel('┐', color)),
                    new RepresantationPoint(new Point(39,1), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(39,2), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(39,3), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(39,4), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(39,5), new Pixel('┘', color))
                };

            body.AddRange(leftCol);
            body.AddRange(rightCol);
            body.AddRange(word);

            return body;
        }
    }
}