﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.MainMenu
{
    internal class MenuPlayButtonMini : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            var color = Color.Gray;

            List<RepresantationPoint> leftCol = new()
                {
                    new RepresantationPoint(new Point(0,0), new Pixel('┌', ColorController.GetRgbColor())),
                    new RepresantationPoint(new Point(0,5), new Pixel('└', ColorController.GetRgbColor()))
                };
            List<RepresantationPoint> rightCol = new()
                {
                    new RepresantationPoint(new Point(20,0), new Pixel('┐', ColorController.GetRgbColor())),
                    new RepresantationPoint(new Point(20,5), new Pixel('┘', ColorController.GetRgbColor()))
                };

            var initialWord = "Выберите сложность";
            var word = StringToPoints(initialWord, color: ColorController.GetRgbColor(), translation: new Point((20 - initialWord.Length) / 2, 2));

            body.AddRange(leftCol);
            body.AddRange(rightCol);
            body.AddRange(word);

            return body;
        }
    }
}