﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.MainMenu
{
    internal class MediumDifficultyButtonBodySelected : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            var color = Color.White;

            List<RepresantationPoint> leftCol = new()
                {
                    new RepresantationPoint(new Point(0,0), new Pixel('┌', color)),
                    new RepresantationPoint(new Point(0,5), new Pixel('└', color))
                };
            List<RepresantationPoint> rightCol = new()
                {
                    new RepresantationPoint(new Point(20,0), new Pixel('┐', color)),
                    new RepresantationPoint(new Point(20,5), new Pixel('┘', color))
                };

            var initialWord = "Средняя";
            var word = StringToPoints(initialWord, color: Color.Yellow, translation: new Point((21 - initialWord.Length) / 2, 2));

            body.AddRange(leftCol);
            body.AddRange(rightCol);
            body.AddRange(word);
            body.Add(new RepresantationPoint(new Point(9, 4), new Pixel('*', ColorController.GetRgbColor())));
            body.Add(new RepresantationPoint(new Point(11, 4), new Pixel('*', ColorController.GetRgbColor())));
            return body;
        }
    }
}