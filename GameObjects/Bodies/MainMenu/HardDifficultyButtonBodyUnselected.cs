﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.MainMenu
{
    internal class HardDifficultyButtonBodyUnselected : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            var color = System.Drawing.Color.Gray;

            List<RepresantationPoint> leftCol = new()
                {
                    new RepresantationPoint(new Point(0,0), new Pixel('┌', color)),
                    new RepresantationPoint(new Point(0,5), new Pixel('└', color))
                };
            List<RepresantationPoint> rightCol = new()
                {
                    new RepresantationPoint(new Point(20,0), new Pixel('┐', color)),
                    new RepresantationPoint(new Point(20,5), new Pixel('┘', color))
                };

            var initialWord = "Сложная";
            var word = StringToPoints(initialWord, color: System.Drawing.Color.Gray, translation: new Point((21 - initialWord.Length) / 2, 2));

            body.AddRange(leftCol);
            body.AddRange(rightCol);
            body.AddRange(word);

            body.Add(new RepresantationPoint(new Point(8, 4), new Pixel('*', color)));
            body.Add(new RepresantationPoint(new Point(10, 4), new Pixel('*', color)));
            body.Add(new RepresantationPoint(new Point(12, 4), new Pixel('*', color)));

            return body;
        }
    }
}