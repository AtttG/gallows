﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.MainMenu
{
    internal class MenuSoundButtonBodySelected : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            var color = Color.White;

            List<RepresantationPoint> leftCol = new()
                {
                    new RepresantationPoint(new Point(0,0), new Pixel('┌', color)),
                    new RepresantationPoint(new Point(0,1), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(0,2), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(0,3), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(0,4), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(0,5), new Pixel('└', color))
                };
            List<RepresantationPoint> rightCol = new()
                {
                    new RepresantationPoint(new Point(40,0), new Pixel('┐', color)),
                    new RepresantationPoint(new Point(40,1), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(40,2), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(40,3), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(40,4), new Pixel(' ', color)),
                    new RepresantationPoint(new Point(40,5), new Pixel('┘', color))
                };

            var initialWord = "Звук";

            var soundColor = (GameWorld.IsSoundEnabled) ? ColorController.GetRgbColor() : Color.Red;

            var word = StringToPoints(initialWord, color: soundColor, translation: new Point((40 - initialWord.Length) / 2, 2));

            body.AddRange(leftCol);
            body.AddRange(rightCol);
            body.AddRange(word);

            return body;
        }
    }
}