﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.MainMenu
{
    internal class MenuExitButtonBodySelected : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            var color = Color.White;

            List<RepresantationPoint> leftCol = new()
                {
                    new RepresantationPoint(new Point(0,0), new Pixel('┌', color)),
                    new RepresantationPoint(new Point(0,5), new Pixel('└', color))
                };
            List<RepresantationPoint> rightCol = new()
                {
                    new RepresantationPoint(new Point(40,0), new Pixel('┐', color)),
                    new RepresantationPoint(new Point(40,5), new Pixel('┘', color))
                };

            var initialWord = "Выход";
            var word = StringToPoints(initialWord, color: Color.Red, translation: new Point((40 - initialWord.Length) / 2, 2));

            body.AddRange(leftCol);
            body.AddRange(rightCol);
            body.AddRange(word);

            return body;
        }
    }
}