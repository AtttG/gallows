﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.MainMenu
{
    internal class MenuPlayButtonBodySelected : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            var color = Color.White;

            List<RepresantationPoint> leftCol = new()
                {
                    new RepresantationPoint(new Point(0,0), new Pixel('┌', color)),
                    new RepresantationPoint(new Point(0,5), new Pixel('└', color))
                };
            List<RepresantationPoint> rightCol = new()
                {
                    new RepresantationPoint(new Point(40,0), new Pixel('┐', color)),
                    new RepresantationPoint(new Point(40,5), new Pixel('┘', color))
                };

            var initialWord = (GameWorld.GameStarted) ? "Продолжить" : "Новая Игра";
            var word = StringToPoints(initialWord, color: ColorController.GetRgbColor(), translation: new Point((40 - initialWord.Length) / 2, 2));

            body.AddRange(leftCol);
            body.AddRange(rightCol);
            body.AddRange(word);

            return body;
        }
    }
}