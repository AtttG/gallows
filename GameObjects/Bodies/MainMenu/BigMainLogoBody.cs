﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.MainMenu
{
    internal class BigMainLogoBody : DynamicBodyObject
    {
        private bool isColorChosen = false;
        private Color _chosenColor = Color.White;

        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            var currentTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            int i = (int)(currentTime % (120 * 50)) / 50;
            Color color1, color2, color3, color4, color5, color6, color7, color8, color9, color10, color11;

            if (i < 11)
            {
                if (isColorChosen)
                {
                    isColorChosen = false;
                }

                color1 = i >= 0 ? Color.Black : _chosenColor;
                color2 = i >= 1 ? Color.Black : _chosenColor;
                color3 = i >= 2 ? Color.Black : _chosenColor;
                color4 = i >= 3 ? Color.Black : _chosenColor;
                color5 = i >= 4 ? Color.Black : _chosenColor;
                color6 = i >= 5 ? Color.Black : _chosenColor;
                color7 = i >= 6 ? Color.Black : _chosenColor;
                color8 = i >= 7 ? Color.Black : _chosenColor;
                color9 = i >= 8 ? Color.Black : _chosenColor;
                color10 = i >= 9 ? Color.Black : _chosenColor;
                color11 = i >= 10 ? Color.Black : _chosenColor;
            }
            else
            {
                if (!isColorChosen)
                {
                    _chosenColor = ColorController.GetRandomColor(75);
                    isColorChosen = true;
                }

                color1 = _chosenColor;
                color2 = i >= 12 ? _chosenColor : Color.Black;
                color3 = i >= 13 ? _chosenColor : Color.Black;
                color4 = i >= 14 ? _chosenColor : Color.Black;
                color5 = i >= 15 ? _chosenColor : Color.Black;
                color6 = i >= 16 ? _chosenColor : Color.Black;
                color7 = i >= 17 ? _chosenColor : Color.Black;
                color8 = i >= 18 ? _chosenColor : Color.Black;
                color9 = i >= 19 ? _chosenColor : Color.Black;
                color10 = i >= 20 ? _chosenColor : Color.Black;
                color11 = i >= 21 ? _chosenColor : Color.Black;
            }

            if (i == 0) { color1 = Color.White; }
            if (i == 1) { color2 = Color.White; }
            if (i == 2) { color3 = Color.White; }
            if (i == 3) { color4 = Color.White; }
            if (i == 4) { color5 = Color.White; }
            if (i == 5) { color6 = Color.White; }
            if (i == 6) { color7 = Color.White; }
            if (i == 7) { color8 = Color.White; }
            if (i == 8) { color9 = Color.White; }
            if (i == 9) { color10 = Color.White; }
            if (i == 10) { color11 = Color.White; }
            if (i == 11) { color1 = Color.White; }
            if (i == 12) { color2 = Color.White; }
            if (i == 13) { color3 = Color.White; }
            if (i == 14) { color4 = Color.White; }
            if (i == 15) { color5 = Color.White; }
            if (i == 16) { color6 = Color.White; }
            if (i == 17) { color7 = Color.White; }
            if (i == 18) { color8 = Color.White; }
            if (i == 19) { color9 = Color.White; }
            if (i == 20) { color10 = Color.White; }
            if (i == 21) { color11 = Color.White; }

            var brackets = new List<RepresantationPoint>()
            {
                new RepresantationPoint(new Point(0,0), new Pixel('┌', Color.White)),
                new RepresantationPoint(new Point(0,11), new Pixel('└', Color.White)),
                new RepresantationPoint(new Point(91,0), new Pixel('┐', Color.White)),
                new RepresantationPoint(new Point(91,11), new Pixel('┘', Color.White))
            };

            var row1 = StringToPoints(@"      ___           ___           ___       ___       ___           ___           ___     " + '\n', color1, new Point(1, 0));
            var row2 = StringToPoints(@"     /\  \         /\  \         /\__\     /\__\     /\  \         /\__\         /\  \    " + '\n', color2, new Point(1, 1));
            var row3 = StringToPoints(@"    /::\  \       /::\  \       /:/  /    /:/  /    /::\  \       /:/ _/_       /::\  \   " + '\n', color3, new Point(1, 2));
            var row4 = StringToPoints(@"   /:/\:\  \     /:/\:\  \     /:/  /    /:/  /    /:/\:\  \     /:/ /\__\     /:/\ \  \  " + '\n', color4, new Point(1, 3));
            var row5 = StringToPoints(@"  /:/  \:\  \   /::\~\:\  \   /:/  /    /:/  /    /:/  \:\  \   /:/ /:/ _/_   _\:\~\ \  \ " + '\n', color5, new Point(1, 4));
            var row6 = StringToPoints(@" /:/__/_\:\__\ /:/\:\ \:\__\ /:/__/    /:/__/    /:/__/ \:\__\ /:/_/:/ /\__\ /\ \:\ \ \__\" + '\n', color6, new Point(1, 5));
            var row7 = StringToPoints(@" \:\  /\ \/__/ \/__\:\/:/  / \:\  \    \:\  \    \:\  \ /:/  / \:\/:/ /:/  / \:\ \:\ \/__/" + '\n', color7, new Point(1, 6));
            var row8 = StringToPoints(@"  \:\ \:\__\        \::/  /   \:\  \    \:\  \    \:\  /:/  /   \::/_/:/  /   \:\ \:\__\  " + '\n', color8, new Point(1, 7));
            var row9 = StringToPoints(@"   \:\/:/  /        /:/  /     \:\  \    \:\  \    \:\/:/  /     \:\/:/  /     \:\/:/  /  " + '\n', color9, new Point(1, 8));
            var row10 = StringToPoints(@"    \::/  /        /:/  /       \:\__\    \:\__\    \::/  /       \::/  /       \::/  /   " + '\n', color10, new Point(1, 9));
            var row11 = StringToPoints(@"     \/__/         \/__/         \/__/     \/__/     \/__/         \/__/         \/__/    " + '\n', color11, new Point(1, 10));

            body.AddRange(row1); body.AddRange(row2); body.AddRange(row3);
            body.AddRange(row4); body.AddRange(row5); body.AddRange(row6);
            body.AddRange(row7); body.AddRange(row8); body.AddRange(row9);
            body.AddRange(row10); body.AddRange(row11); body.AddRange(brackets);

            return body;
        }
    }
}