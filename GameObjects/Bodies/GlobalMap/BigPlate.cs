﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class BigPlate : DynamicBodyObject
    {
        private bool isColorChosen = false;
        private Color _chosenColor = Color.White;

        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            Color color1, color2, color3, color4, color5, color6, color7, color8, color9, color10, color11;

            var currentTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            int i = (int)(currentTime % (120 * 50)) / 50;

            if (i < 11)
            {
                if (isColorChosen)
                {
                    isColorChosen = false;
                }

                color1 = i >= 0 ? Color.Black : ColorController.GetDarkRgbColor();
                color2 = i >= 1 ? Color.Black : ColorController.GetDarkRgbColor();
                color3 = i >= 2 ? Color.Black : ColorController.GetDarkRgbColor();
                color4 = i >= 3 ? Color.Black : ColorController.GetDarkRgbColor();
                color5 = i >= 4 ? Color.Black : ColorController.GetDarkRgbColor();
                color6 = i >= 5 ? Color.Black : ColorController.GetDarkRgbColor();
                color7 = i >= 6 ? Color.Black : ColorController.GetDarkRgbColor();
                color8 = i >= 7 ? Color.Black : ColorController.GetDarkRgbColor();
                color9 = i >= 8 ? Color.Black : ColorController.GetDarkRgbColor();
                color10 = i >= 9 ? Color.Black : ColorController.GetDarkRgbColor();
                color11 = i >= 10 ? Color.Black : ColorController.GetDarkRgbColor();
            }
            else
            {
                if (!isColorChosen)
                {
                    _chosenColor = ColorController.GetRandomColor(75);
                    isColorChosen = true;
                }

                color1 = ColorController.GetDarkRgbColor();
                color2 = i >= 12 ? ColorController.GetDarkRgbColor() : Color.Black;
                color3 = i >= 13 ? ColorController.GetDarkRgbColor() : Color.Black;
                color4 = i >= 14 ? ColorController.GetDarkRgbColor() : Color.Black;
                color5 = i >= 15 ? ColorController.GetDarkRgbColor() : Color.Black;
                color6 = i >= 16 ? ColorController.GetDarkRgbColor() : Color.Black;
                color7 = i >= 17 ? ColorController.GetDarkRgbColor() : Color.Black;
                color8 = i >= 18 ? ColorController.GetDarkRgbColor() : Color.Black;
                color9 = i >= 19 ? ColorController.GetDarkRgbColor() : Color.Black;
                color10 = i >= 20 ? ColorController.GetDarkRgbColor() : Color.Black;
                color11 = i >= 21 ? ColorController.GetDarkRgbColor() : Color.Black;
            }

            if (i == 0) { color1 = Color.White; }
            if (i == 1) { color2 = Color.White; }
            if (i == 2) { color3 = Color.White; }
            if (i == 3) { color4 = Color.White; }
            if (i == 4) { color5 = Color.White; }
            if (i == 5) { color6 = Color.White; }
            if (i == 6) { color7 = Color.White; }
            if (i == 7) { color8 = Color.White; }
            if (i == 8) { color9 = Color.White; }
            if (i == 9) { color10 = Color.White; }
            if (i == 10) { color11 = Color.White; }
            if (i == 11) { color1 = Color.White; }
            if (i == 12) { color2 = Color.White; }
            if (i == 13) { color3 = Color.White; }
            if (i == 14) { color4 = Color.White; }
            if (i == 15) { color5 = Color.White; }
            if (i == 16) { color6 = Color.White; }
            if (i == 17) { color7 = Color.White; }
            if (i == 18) { color8 = Color.White; }
            if (i == 19) { color9 = Color.White; }
            if (i == 20) { color10 = Color.White; }
            if (i == 21) { color11 = Color.White; }

            body.AddRange(ObjectBody.StringToPoints("  ██               ██\n", color1, new Point(0, 0)));
            body.AddRange(ObjectBody.StringToPoints("████▄   ▄▄▄▄▄▄▄   ▄████\n", color2, new Point(0, 1)));
            body.AddRange(ObjectBody.StringToPoints("   ▀▀█▄█████████▄█▀▀\n", color3, new Point(0, 2)));
            body.AddRange(ObjectBody.StringToPoints("     █████████████\n", color4, new Point(0, 3)));
            body.AddRange(ObjectBody.StringToPoints("     ██▀▀▀███▀▀▀██\n", color5, new Point(0, 4)));
            body.AddRange(ObjectBody.StringToPoints("     ██   ███   ██\n", color6, new Point(0, 5)));
            body.AddRange(ObjectBody.StringToPoints("     █████▀▄▀█████\n", color7, new Point(0, 6)));
            body.AddRange(ObjectBody.StringToPoints("      ███████████\n", color8, new Point(0, 7)));
            body.AddRange(ObjectBody.StringToPoints("  ▄▄▄██  █▀█▀█  ██▄▄▄\n", color9, new Point(0, 8)));
            body.AddRange(ObjectBody.StringToPoints("  ▀▀██           ██▀▀\n", color10, new Point(0, 9)));
            body.AddRange(ObjectBody.StringToPoints("    ▀▀           ▀▀\n", color11, new Point(0, 10)));

            return body;
        }
    }
}