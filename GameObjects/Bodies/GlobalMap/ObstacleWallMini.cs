﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class ObstacleWallMini : StaticBodyObject
    {
        private Color _color;

        public ObstacleWallMini(Color? color = null)
        {
            if (color.HasValue)
            {
                _color = color.Value;
            }
            else
            {
                _color = Color.White;
            }
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            body.AddRange(ObjectBody.StringToPoints("█", _color));

            return body;
        }
    }
}