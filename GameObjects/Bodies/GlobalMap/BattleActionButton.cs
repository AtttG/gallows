﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class BattleActionButton : DynamicBodyObject, IRenderableObject
    {
        private BattleAction _battleAction;

        public List<RepresantationPoint> Represantation
        {
            get
            {
                return GetBody();
            }
        }

        public void SetAction(BattleAction action)
        {
            _battleAction = action;
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            if (_battleAction != null)
            {
                body.AddRange(ObjectBody.StringToPoints($"  " + _battleAction.Name, Color.White, new Point(0, 1)));

                if (_battleAction.Damage >= 0)
                {
                    body.AddRange(ObjectBody.StringToPoints($"  Урон: {_battleAction.Damage} ХП", Color.Red, new Point(0, 2)));
                }
                else
                {
                    body.AddRange(ObjectBody.StringToPoints($"  Иссцеление: {-_battleAction.Damage} ХП", Color.Red, new Point(0, 2)));
                }

                if (_battleAction.Cost >= 0)
                {
                    body.AddRange(ObjectBody.StringToPoints($"  Затраты: {_battleAction.Cost} ЭН", Color.Cyan, new Point(0, 3)));
                }
                else
                {
                    body.AddRange(ObjectBody.StringToPoints($"  Восстановление: {-_battleAction.Cost} ЭН", Color.Cyan, new Point(0, 3)));
                }
            }

            body.AddRange(ObjectBody.StringToPoints($"┌                          ┐", Color.White, new Point(0, 0)));
            body.AddRange(ObjectBody.StringToPoints($"└                          ┘", Color.White, new Point(0, 4)));

            return body;
        }
    }
}