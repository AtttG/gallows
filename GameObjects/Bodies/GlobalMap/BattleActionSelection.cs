﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class BattleActionSelection : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            if (GameWorld.GlobalMap.IsExecutingAction)
            {
                body.AddRange(ObjectBody.StringToPoints(
                $"╔══════════════════════════╗" + '\n' +
                $"║                          ║" + '\n' +
                $"║                          ║" + '\n' +
                $"║                          ║" + '\n' +
                $"╚══════════════════════════╝", Color.Red, new Point(0, 0)));
            }
            else
            {
                body.AddRange(ObjectBody.StringToPoints(
                $"╔══════════════════════════╗" + '\n' +
                $"║                          ║" + '\n' +
                $"║                          ║" + '\n' +
                $"║                          ║" + '\n' +
                $"╚══════════════════════════╝", ColorController.GetRgbColor(), new Point(0, 0)));
            }

            //body.AddRange(ObjectBody.StringToPoints(, ColorController.GetRgbColor(), new Point(0, 4)));

            return body;
        }
    }
}