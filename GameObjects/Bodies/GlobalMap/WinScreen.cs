﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class WinScreen : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            body.AddRange(ObjectBody.StringToPoints(
                "╔─────────────────────────────────────────────────────────────────────────────────────────╗" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "╚─────────────────────────────────────────────────────────────────────────────────────────╝" + '\n', ColorController.GetRgbColor()));

            body.AddRange(ObjectBody.StringToPoints(
                "                                                                                           " + '\n'
              + "                                       Вы выиграли!                                        " + '\n'
              + "                                                                                           " + '\n'
              + $"    Длительность сессии: {(int)(GameWorld.GameEndTime - GameWorld.GameStartTime).TotalSeconds} сек" + '\n'
              + "                                                                                           " + '\n'
              + $"                              " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "    Нажмите любую клавишу чтобы выйти...                                                   " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n', Color.LightGreen));

            return body;
        }
    }
}