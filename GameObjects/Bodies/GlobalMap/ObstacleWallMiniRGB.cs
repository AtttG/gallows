﻿using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class ObstacleWallMiniRGB : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            body.AddRange(ObjectBody.StringToPoints("█", ColorController.GetDarkRgbColor()));

            return body;
        }
    }
}