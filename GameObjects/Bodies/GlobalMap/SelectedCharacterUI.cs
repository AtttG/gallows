﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class SelectedCharacterUI : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            if (GameWorld.GlobalMap.IsExecutingAction)
            {
                body.AddRange(ObjectBody.StringToPoints(
                    "╔═            ═╗", Color.Red));
                body.AddRange(ObjectBody.StringToPoints(
                    "╚═            ═╝", Color.Red, new Point(0, 6)));
            }
            else
            {
                body.AddRange(ObjectBody.StringToPoints(
                    "┌─            ─┐", ColorController.GetRgbColor()));
                body.AddRange(ObjectBody.StringToPoints(
                    "└─            ─┘", ColorController.GetRgbColor(), new Point(0, 6)));
            }

            return body;
        }
    }
}