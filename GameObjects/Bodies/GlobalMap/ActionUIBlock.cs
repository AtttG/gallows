﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class ActionUIBlock : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            body.AddRange(ObjectBody.StringToPoints("┌─  ПАНЕЛЬ ИНФОРМАЦИИ:                                                                                                   ─┐", Color.Gray));
            //body.AddRange(ObjectBody.StringToPoints("    [ИНФО] Используйте стрелки для ходьбы по карте                                                                         ", ColorController.GetRgbColor(), new Point(0, 3)));
            body.AddRange(ObjectBody.StringToPoints("└─                                                                                                                       ─┘", Color.Gray, new Point(0, 6)));

            return body;
        }
    }
}