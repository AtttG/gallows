﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class CharacterUI : DynamicBodyObject
    {
        private IDiableCharacter _character;

        public CharacterUI(IDiableCharacter character)
        {
            _character = character;
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            //body.Add(new RepresantationPoint(new Point(0, 0), new Pixel('┌', Color.White)));

            body.AddRange(ObjectBody.StringToPoints("┌──────────┐"));

            body.AddRange(ObjectBody.StringToPoints(_character.Name, _character.CharacterColor, new Point(1, 0)));

            //body.Add(new RepresantationPoint(new Point(11, 0), new Pixel('┐', Color.White)));

            var hpRatio = (float)_character.HP / _character.MaxHP;

            int helthIndex = (int)Math.Round(hpRatio * 10f);

            body.Add(new RepresantationPoint(new Point(0, 1), new Pixel('[', Color.White)));

            for (int i = 1; i < helthIndex + 1; i++)
            {
                body.Add(new RepresantationPoint(new Point(i, 1), new Pixel('=', Color.FromArgb(255, 255, 25, 25))));
            }

            body.Add(new RepresantationPoint(new Point(11, 1), new Pixel(']', Color.White)));

            body.AddRange(ObjectBody.StringToPoints("└──────────┘", translation: new Point(0, 2)));

            body.AddRange(ObjectBody.StringToPoints($"HP:{_character.HP}/{_character.MaxHP}", Color.FromArgb(255, 255, 25, 25), new Point(1, 2)));

            return body;
        }
    }
}