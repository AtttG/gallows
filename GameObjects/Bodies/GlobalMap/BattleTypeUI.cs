﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.GameScenes;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class BattleTypeUI : DynamicBodyObject
    {
        private int battleIndex;
        private DateTime _lastUpdate = DateTime.MinValue;

        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            var color = (GameWorld.GlobalMap.IsPlayerInTrap) ? Color.Red : Color.Gray;

            if ((GameWorld.GlobalMap.ExitingBattleMode) || GameWorld.GlobalMap.State != GlobalMapScene.SubState.BATTLE)
            {
                if (battleIndex > 0 && (DateTime.Now - _lastUpdate).TotalMilliseconds > 50)
                {
                    _lastUpdate = DateTime.Now;
                    battleIndex--;
                }
            }
            else if (GameWorld.GlobalMap.State == GlobalMapScene.SubState.BATTLE || GameWorld.GlobalMap.EnteringBattleMode)
            {
                if (battleIndex < 23 && (DateTime.Now - _lastUpdate).TotalMilliseconds > 50)
                {
                    battleIndex++;
                    _lastUpdate = DateTime.Now;
                }
            }

            for (int i = 1; i <= battleIndex; i++)
            {
                body.AddRange(ObjectBody.StringToPoints(" ----------------------------------------------------------------------------------------- ", Color.FromArgb(255, 50, 50, 50), new Point(0, i)));
                body.AddRange(ObjectBody.StringToPoints(" |-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_|-|_| ", Color.FromArgb(255, 50, 50, 50), new Point(0, i)));
            }

            body.AddRange(ObjectBody.StringToPoints(
                "┌─[   БОЙ   ]──[-----------------------------------------------------------]──[ ЭНЕРГИЯ ]─┐" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "└─────────────────────────────────────────────────────────────────────────────────────────┘" + '\n', color));

            int ratio = (int)Math.Round(((double)GameWorld.EnergyLeft / GameWorld.MaxEnergy) * 59);

            string energyBar = "                ";

            for (int i = 0; i < ratio; i++)
            {
                energyBar += "=";
            }
            body.AddRange(ObjectBody.StringToPoints(energyBar, Color.Cyan));

            body.AddRange(ObjectBody.StringToPoints($@"                {GameWorld.EnergyLeft}\{GameWorld.MaxEnergy}", Color.Cyan));

            if (GameWorld.GlobalMap.IsPlayersMove)
            {
                body.AddRange(ObjectBody.StringToPoints($@"                Ваш ход", Color.LightGreen, new Point(0, 2)));

                if (GameWorld.GlobalMap.IsExecutingAction)
                {
                    body.AddRange(ObjectBody.StringToPoints($@"                Выберите цель для действия", Color.White, new Point(0, 4)));
                }
                else
                {
                    body.AddRange(ObjectBody.StringToPoints($@"                Выберите персонажа и действие", Color.White, new Point(0, 4)));
                }

                var descriptionColor = Color.White;

                if (GameWorld.GlobalMap.IsExecutingAction)
                {
                    descriptionColor = Color.Gray;
                }

                if (GameWorld.GlobalMap.CurrentAction == GlobalMapScene.InBattleAction.LEFT_ACTION)
                {
                    body.AddRange(ObjectBody.StringToPoints($@"{GameWorld.GlobalMap.SelectedGameCharacter.LeftAction.Description}", descriptionColor, new Point(16, 6)));
                }
                else
                {
                    body.AddRange(ObjectBody.StringToPoints($@"{GameWorld.GlobalMap.SelectedGameCharacter.RightAction.Description}", descriptionColor, new Point(16, 6)));
                }
            }
            else
            {
                body.AddRange(ObjectBody.StringToPoints($@"                Ход оппонента", Color.Red, new Point(0, 2)));
            }

            return body;
        }
    }
}