﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class GlobalMapLogger : DynamicBodyObject, IRenderableObject
    {
        private int _width;
        private int _height;

        public GlobalMapLogger(int width = 50, int height = 5)
        {
            _width = width;
            _height = height;
        }

        public enum MessageType
        { COMMON, GOOD, BAD }

        private List<LoggerMessage> _logQueue = new();

        public List<RepresantationPoint> Represantation
        {
            get
            {
                var body = InitializeBody();
                foreach (var o in body)
                {
                    o.Move(TargetPosition.X, TargetPosition.Y);
                }
                return body;
            }
        }

        public void AddMessage(string message, MessageType messageType)
        {
            if (message.Length >= _width)
            {
                message = message.Substring(0, _width);
            }
            _logQueue.Add(new LoggerMessage(message, messageType));
        }

        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            var counter = 0;

            for (int i = _logQueue.Count - _height; i < _logQueue.Count; i++)
            {
                if (i >= 0)
                {
                    if (_logQueue[i].Type == MessageType.GOOD)
                    {
                        body.AddRange(ObjectBody.StringToPoints(_logQueue[i].Message, ColorController.GetRgbColor(), new Point(0, counter)));
                    }
                    if (_logQueue[i].Type == MessageType.BAD)
                    {
                        body.AddRange(ObjectBody.StringToPoints(_logQueue[i].Message, Color.FromArgb(255, 255, 100, 100), new Point(0, counter)));
                    }
                    if (_logQueue[i].Type == MessageType.COMMON)
                    {
                        body.AddRange(ObjectBody.StringToPoints(_logQueue[i].Message, Color.FromArgb(255, 200, 200, 200), new Point(0, counter)));
                    }
                }
                counter++;
            }

            return body;
        }
    }

    internal class LoggerMessage
    {
        public string Message;
        public GlobalMapLogger.MessageType Type;

        public LoggerMessage(string message, GlobalMapLogger.MessageType type)
        {
            Message = message;
            Type = type;
        }
    }
}