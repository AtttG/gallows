﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class LoseScreen : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            body.AddRange(ObjectBody.StringToPoints(
                "╔═──═──────────────────────────────────────────────────────────────────────────────────═─═╗" + '\n'
              + "│                                                                                         │" + '\n'
              + "║                                                                                         ║" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                         ┌     ┐                                         │" + '\n'
              + "│                                           xxx                                           │" + '\n'
              + "│                                         └     ┘                                         │" + '\n'
              + @"│                                                \                                        │" + '\n'
              + @"│                                                 \________________                       │" + '\n'
              + "│                                                   Отряд уничтожен                       │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "║                                                                                         ║" + '\n'
              + "│                                                                                         │" + '\n'
              + "╚═─═───────────────────────────────────────────────────────────────────────────────────═─═╝" + '\n', Color.Red));

            body.AddRange(ObjectBody.StringToPoints(
                "                                                                                           " + '\n'
              + "                                      Вы проиграли!                                        " + '\n'
              + "                                                                                           " + '\n'
              + $"    Длительность сессии: {(int)(GameWorld.GameEndTime - GameWorld.GameStartTime).TotalSeconds} сек" + '\n'
              + "                                                                                           " + '\n'
              + $"                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "    Нажмите любую клавишу чтобы выйти...                                                   " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n'
              + "                                                                                           " + '\n', Color.Red));

            return body;
        }
    }
}