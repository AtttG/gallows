﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class SparkLeft : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            body.AddRange(ObjectBody.StringToPoints("<", Color.Red));

            return body;
        }
    }
}