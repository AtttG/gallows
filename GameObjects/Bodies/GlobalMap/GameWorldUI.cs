﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class GameWorldUI : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            var color = (GameWorld.GlobalMap.IsPlayerInTrap) ? Color.Red : Color.Gray;

            body.AddRange(ObjectBody.StringToPoints(
                "┌─[   ИГРОВАЯ КАРТА   ]───────────────────────────────────────────────────────────────────┐" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "│                                                                                         │" + '\n'
              + "└─────────────────────────────────────────────────────────────────────────────────────────┘" + '\n', color));

            //body.AddRange(ObjectBody.StringToPoints("└─                                                                                       ─┘", Color.Gray, new Point(0, 24)));

            return body;
        }
    }
}