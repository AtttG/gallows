﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class PowerCabel : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var currentTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            int currentTimeIndex = (int)(currentTime % (5 * 300)) / 300;

            var body = new List<RepresantationPoint>();
            body.AddRange(ObjectBody.StringToPoints(
              "[█]" + '\n'
            + "[█]" + '\n'
            + "[█]" + '\n'
            + "[█]" + '\n'
            + "[█]" + '\n', Color.DarkGray));

            var color = ColorController.GetRgbColor();

            switch (currentTimeIndex)
            {
                case 0:

                    break;

                case 1:
                    body.AddRange(ObjectBody.StringToPoints(
                      " ▓" + '\n'
                    + " ▓" + '\n', color));
                    break;

                case 2:
                    body.AddRange(ObjectBody.StringToPoints(
                    "" + '\n'
                    + " ▓" + '\n'
                    + " ▓", color));
                    break;

                case 3:
                    body.AddRange(ObjectBody.StringToPoints(
                    "" + '\n'
                    + '\n'
                    + " ▓" + '\n'
                    + " ▓" + '\n', color));
                    break;

                case 4:
                    body.AddRange(ObjectBody.StringToPoints(
                    "" + '\n'
                    + '\n'
                    + '\n'
                    + " ▓" + '\n'
                    + " ▓" + '\n', color));
                    break;
            }

            return body;
        }
    }
}