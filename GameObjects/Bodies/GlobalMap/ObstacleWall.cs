﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class ObstacleWall : StaticBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();

            body.AddRange(ObjectBody.StringToPoints("[_]", color: Color.SandyBrown));

            return body;
        }
    }
}