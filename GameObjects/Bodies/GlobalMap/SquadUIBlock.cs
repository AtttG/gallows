﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class SquadUIBlock : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            body.AddRange(ObjectBody.StringToPoints("┌─   ОТРЯД:   ─┐", Color.Gray));
            body.AddRange(ObjectBody.StringToPoints("└─            ─┘", Color.Gray, new Point(0, 24)));

            return body;
        }
    }
}