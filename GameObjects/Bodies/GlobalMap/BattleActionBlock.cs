﻿using System.Drawing;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects.Bodies.GlobalMap
{
    internal class BattleActionBlock : DynamicBodyObject
    {
        public override List<RepresantationPoint> InitializeBody()
        {
            var body = new List<RepresantationPoint>();
            body.AddRange(ObjectBody.StringToPoints("┌─  ДОСТУПНЫЕ ДЕЙСТВИЯ:                                    ─┐", Color.Gray));
            body.AddRange(ObjectBody.StringToPoints("└─                                                         ─┘", Color.Gray, new Point(0, 6)));

            return body;
        }
    }
}