﻿using System.Drawing;
using TheGallowsSurvival.GameScene;

namespace TheGallowsSurvival.GameObjects.Bodies
{
    internal class Enemy : GameObject, IDiableCharacter
    {
        private string _name;
        private int _HP;
        private int _maxHP;
        private Color _color;

        public Enemy(List<ObjectBody> body, string name, int maxHP, Color color) : base(body)
        {
            _name = name;
            _HP = _maxHP = maxHP;
            _color = color;
        }

        public bool TakeDamage(int damage)
        {
            _HP -= damage;

            if (_HP > _maxHP)
            {
                _HP = _maxHP;
            }

            return (_HP <= 0);
        }

        public int HP
        { get { return _HP; } }

        public int MaxHP
        { get { return _maxHP; } }

        public Color CharacterColor
        { get { return _color; } }

        string IDiableCharacter.Name { get { return _name; } }
    }

    internal class Outlaw : Enemy
    {
        public Outlaw(List<ObjectBody> body, string name, int maxHP, Color color) : base(body, name, maxHP, color)
        {
        }
    }

    internal class EnemySquad : GameObject, IDynamicGameObject
    {
        private Point _patrolPoint;
        private int _patrolRange;
        private ObjectBody _myPoint;
        private DateTime _lastMovementTime = DateTime.MinValue;
        private bool _lastStepIsHorizontal = false;
        private bool _inPlayer = false;
        private bool _isFollowing;
        private int _speed;

        public Enemy LeftEnemy;
        public Enemy MidEnemy;
        public Enemy RightEnemy;

        public string Name;

        public EnemySquad(List<ObjectBody> body, Point patrolPoint, int patrolRange, string name, int speed, Enemy leftEnemy, Enemy midEnemy, Enemy rightEnemy) : base(body)
        {
            _myPoint = body[0];
            _patrolPoint = patrolPoint;
            _patrolRange = patrolRange;
            _speed = speed;
            Name = name;

            LeftEnemy = leftEnemy;
            MidEnemy = midEnemy;
            RightEnemy = rightEnemy;
        }

        public bool IsBusy()
        {
            return false;
        }

        private bool _isDead = false;

        public void Die()
        {
            _isDead = true;
        }

        public void Update()
        {
            if (!_isDead && !GameWorld.GlobalMap.GameEnded)
            {
                _inPlayer = false;
                Point? foundClosestPoint = null;
                bool isMoved = false;

                foreach (var point in GameWorld.GlobalMap.PlayerOnMap.Represantation)
                {
                    if (point.Position.X - _myPoint.GetBody()[0].Position.X == 0 && point.Position.Y - _myPoint.GetBody()[0].Position.Y == 0)
                    {
                        // Достиг игрока
                        foundClosestPoint = null;
                        _inPlayer = true;

                        GameWorld.GlobalMap.StartBattle(this);

                        break;
                    }
                    else
                    {
                        if (Math.Abs(point.Position.X - _myPoint.GetBody()[0].Position.X) < 100 && Math.Abs(point.Position.Y - _myPoint.GetBody()[0].Position.Y) < 50)
                        {
                            if (foundClosestPoint.HasValue)
                            {
                                if ((Math.Abs(point.Position.X - _myPoint.GetBody()[0].Position.X) + Math.Abs(point.Position.Y - _myPoint.GetBody()[0].Position.Y)) < (Math.Abs(foundClosestPoint.Value.Y - _myPoint.GetBody()[0].Position.Y) + Math.Abs(foundClosestPoint.Value.X - _myPoint.GetBody()[0].Position.X)))
                                {
                                    foundClosestPoint = point.Position;
                                }
                            }
                            else
                            {
                                foundClosestPoint = point.Position;
                            }
                        }
                    }
                }

                if (foundClosestPoint.HasValue)
                {
                    // Перемещение
                    if ((Math.Abs(foundClosestPoint.Value.X - _patrolPoint.X) <= (_patrolRange * 1.5)) && (Math.Abs(foundClosestPoint.Value.Y - _patrolPoint.Y) <= _patrolRange))
                    {
                        if (_isFollowing == false)
                        {
                            GameWorld.GlobalMap.GameLogger.AddMessage($"Отряд {Name} преследует игрока", GlobalMap.GlobalMapLogger.MessageType.BAD);
                        }

                        _isFollowing = true;

                        if ((DateTime.Now - _lastMovementTime).TotalMilliseconds > _speed)
                        {
                            if (_lastStepIsHorizontal)
                            {
                                if (foundClosestPoint.Value.Y < _myPoint.GetBody()[0].Position.Y)
                                {
                                    _myPoint.Move(0, -1);
                                }
                                else if (foundClosestPoint.Value.Y > _myPoint.GetBody()[0].Position.Y)
                                {
                                    _myPoint.Move(0, 1);
                                }
                                else if (foundClosestPoint.Value.X < _myPoint.GetBody()[0].Position.X)
                                {
                                    _myPoint.Move(-1, 0);
                                }
                                else if (foundClosestPoint.Value.X > _myPoint.GetBody()[0].Position.X)
                                {
                                    _myPoint.Move(1, 0);
                                }
                            }
                            else
                            {
                                if (foundClosestPoint.Value.X < _myPoint.GetBody()[0].Position.X)
                                {
                                    _myPoint.Move(-1, 0);
                                }
                                else if (foundClosestPoint.Value.X > _myPoint.GetBody()[0].Position.X)
                                {
                                    _myPoint.Move(1, 0);
                                }
                                else if (foundClosestPoint.Value.Y < _myPoint.GetBody()[0].Position.Y)
                                {
                                    _myPoint.Move(0, -1);
                                }
                                else if (foundClosestPoint.Value.Y > _myPoint.GetBody()[0].Position.Y)
                                {
                                    _myPoint.Move(0, 1);
                                }
                            }

                            isMoved = true;
                            _lastStepIsHorizontal = !_lastStepIsHorizontal;
                            _lastMovementTime = DateTime.Now;
                        }
                    }
                }
                if (!_inPlayer && !isMoved)
                {
                    if (!(_patrolPoint.X - _myPoint.GetBody()[0].Position.X == 0 && _patrolPoint.Y - _myPoint.GetBody()[0].Position.Y == 0))
                    {
                        if ((DateTime.Now - _lastMovementTime).TotalMilliseconds > _speed)
                        {
                            if (_lastStepIsHorizontal)
                            {
                                if (_patrolPoint.Y < _myPoint.GetBody()[0].Position.Y)
                                {
                                    _myPoint.Move(0, -1);
                                }
                                else if (_patrolPoint.Y > _myPoint.GetBody()[0].Position.Y)
                                {
                                    _myPoint.Move(0, 1);
                                }
                                else if (_patrolPoint.X < _myPoint.GetBody()[0].Position.X)
                                {
                                    _myPoint.Move(-1, 0);
                                }
                                else if (_patrolPoint.X > _myPoint.GetBody()[0].Position.X)
                                {
                                    _myPoint.Move(1, 0);
                                }
                            }
                            else
                            {
                                if (_patrolPoint.X < _myPoint.GetBody()[0].Position.X)
                                {
                                    _myPoint.Move(-1, 0);
                                }
                                else if (_patrolPoint.X > _myPoint.GetBody()[0].Position.X)
                                {
                                    _myPoint.Move(1, 0);
                                }
                                else if (_patrolPoint.Y < _myPoint.GetBody()[0].Position.Y)
                                {
                                    _myPoint.Move(0, -1);
                                }
                                else if (_patrolPoint.Y > _myPoint.GetBody()[0].Position.Y)
                                {
                                    _myPoint.Move(0, 1);
                                }
                            }

                            if (_isFollowing == true)
                            {
                                GameWorld.GlobalMap.GameLogger.AddMessage($"Отряд {Name} потерял игрока из виду...", GlobalMap.GlobalMapLogger.MessageType.COMMON);
                            }

                            _isFollowing = false;
                            isMoved = true;
                            _lastStepIsHorizontal = !_lastStepIsHorizontal;
                            _lastMovementTime = DateTime.Now;
                        }
                    }
                }
            }
        }
    }
}