﻿namespace TheGallowsSurvival.GameObjects
{
    internal interface IAnimatedObject
    {
        public void Animate();
    }
}