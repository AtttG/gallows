﻿using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameObjects
{
    internal interface IRenderableObject
    {
        public List<RepresantationPoint> Represantation
        {
            get;
        }
    }
}