﻿using System.Drawing;
using TheGallowsSurvival.GameObjects;
using TheGallowsSurvival.GameObjects.Bodies;
using TheGallowsSurvival.GameObjects.Bodies.Characters;
using TheGallowsSurvival.GameObjects.Bodies.Enemies;
using TheGallowsSurvival.GameObjects.Bodies.General;
using TheGallowsSurvival.GameObjects.Bodies.GlobalMap;
using TheGallowsSurvival.GameScene;

namespace TheGallowsSurvival.GameScenes
{
    internal class GlobalMapScene : IGameScene
    {
        public InBattleAction CurrentAction = InBattleAction.LEFT_ACTION;

        public InBattleCharacterSelection CurrentCharacter;

        public EnemySquad Enemy;

        public InBattleExecutionSelection ExecutionTarget = InBattleExecutionSelection.RIGHT_UP;

        public GlobalMapLogger GameLogger;

        public bool IsExecutingAction;

        public bool IsPlayerInTrap;

        public GameCharacter LeftCharacter;

        public BasicBodyOwner LeftCharacterBody;

        public BasicBodyOwner LeftCharacterUI;

        public GameCharacter MidCharacter;

        public BasicBodyOwner MidCharacterBody;

        public BasicBodyOwner MidCharacterUI;

        public BasicBodyOwner PlayerOnMap;

        public GameCharacter RightCharacter;

        public BasicBodyOwner RightCharacterBody;

        public BasicBodyOwner RightCharacterUI;

        public GameCharacter SelectedGameCharacter;

        public SubState State = SubState.MOVING;

        private BasicBodyOwner _actionsPanelSmall;

        private BasicBodyOwner _actionsPanelUIBlock;

        private DateTime _animationStart;

        private BasicBodyOwner _battleTypeUI;

        private BasicBodyOwner _battleUIBlock;

        private bool _busy = false;

        private List<BasicBodyOwner> _collisionObjects = new();

        private EnemySquad _currentOpponent = null;

        private BasicBodyOwner _dateTime;

        private List<IDynamicGameObject> _dynamicGameObjects = new();

        private BasicBodyOwner _enemySquadUIBlock;

        public bool EnteringBattleMode = false;

        private int _enteringCount;

        public bool ExitingBattleMode = false;

        private int _exitingCount;

        private DateTime? _gameOverTime;

        private BasicBodyOwner _gameWorldUI;

        private long _lastHitByTrap = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

        private BattleActionButton _leftBattleAction;

        private List<BasicBodyOwner> _noneCollisionObjects = new();

        private BattleActionButton _rightBattleAction;

        private BasicBodyOwner _selectionUI;

        private BasicBodyOwner _soundLogo;

        private bool _squadAlreadyDead;

        private BasicBodyOwner _squadUIBlock;

        private List<StraightTrap> _traps = new();

        private BasicBodyOwner LeftEnemyUI;

        private BasicBodyOwner MidEnemyUI;

        private BasicBodyOwner RightEnemyUI;

        private BasicBodyOwner _battleActionSelectionUI;

        private BasicBodyOwner _gameMainTarget;

        private DateTime _lastTimePlayerMoved = DateTime.MaxValue;

        public bool IsPlayersMove = true;

        private List<Point> SeletionUIPositions = new()
        {
            new Point(0,2),
            new Point(0,9),
            new Point(0,16),
            new Point(107,2),
            new Point(107,9),
            new Point(107,16),
        };

        public enum InBattleAction
        { LEFT_ACTION, RIGHT_ACTION }

        public enum InBattleCharacterSelection
        { LEFT_CHARACTER, MID_CHARACTER, RIGHT_CHARACTER }

        public enum InBattleExecutionSelection
        { LEFT_UP, LEFT_MID, LEFT_DOWN, RIGHT_UP, RIGHT_MID, RIGHT_DOWN }

        public enum PlayerMovementOptions
        { UP, RIGHT, DOWN, LEFT }

        public enum SubState
        { MOVING, BATTLE, END_SCREEN }

        public enum TargetSelectionSwitch
        { RIGHT, LEFT, DOWN, UP }

        public bool IsBusy
        {
            get
            {
                foreach (var gameObject in _dynamicGameObjects)
                {
                    if (gameObject.IsBusy())
                    {
                        return true;
                    }
                }

                return _busy;
            }
        }

        public void BuildGameWorld()
        {
            int collidionObjectsCounter = 0;
            int noneCollisionObjectsCounter = 0;
            int trapsCounter = 0;

            int minTrapSpeed = 50;
            if (GameWorld.SelectedDifficulty == GameDifficulty.Medium)
            {
                minTrapSpeed = 40;
            }
            else if (GameWorld.SelectedDifficulty == GameDifficulty.Hard)
            {
                minTrapSpeed = 25;
            }

            Point circleCenter = new(GameWorld.Screen.OFFSET.X + 44, GameWorld.Screen.OFFSET.Y);

            _noneCollisionObjects.AddRange(
            new List<BasicBodyOwner>()
            {
                    new BasicBodyOwner(new List<ObjectBody>() { new BigPlate() })
            }
            );
            _noneCollisionObjects[noneCollisionObjectsCounter].MoveTo(circleCenter.X + 20, circleCenter.Y - 70);
            noneCollisionObjectsCounter++;

            _noneCollisionObjects.AddRange(
            new List<BasicBodyOwner>()
            {
                                new BasicBodyOwner(new List<ObjectBody>() { new BigPlate() })
            }
            );
            _noneCollisionObjects[noneCollisionObjectsCounter].MoveTo(circleCenter.X - 42, circleCenter.Y - 70);
            noneCollisionObjectsCounter++;

            var random = new Random();

            for (int i = 0; i < 360; i++)
            {
                int x = circleCenter.X + (int)Math.Round(20 * 2 * Math.Sin(i));
                int y = circleCenter.Y + (int)Math.Round(20 * Math.Cos(i));

                if (!(!(y > 0) && (x < (circleCenter.X + 16) && (x > (circleCenter.X - 16)))))
                {
                    _collisionObjects.AddRange(
                    new List<BasicBodyOwner>()
                    {
                    new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMiniRGB() })
                    }
                    );
                    _collisionObjects[collidionObjectsCounter].MoveTo(x, y);
                    collidionObjectsCounter++;
                }
            }

            for (int i = 0; i < 360; i++)
            {
                int x = circleCenter.X + (int)Math.Round(10 * 2 * Math.Sin(i));
                int y = circleCenter.Y + (int)Math.Round(10 * Math.Cos(i));

                _noneCollisionObjects.AddRange(
               new List<BasicBodyOwner>()
               {
                   new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMini(System.Drawing.Color.DarkGray) })
               }
               );
                _noneCollisionObjects[noneCollisionObjectsCounter].MoveTo(x, y);
                noneCollisionObjectsCounter++;
            }

            var rightWallX = (circleCenter.X + 13);
            var leftWallX = (circleCenter.X - 15);

            for (int x = -1; x < 2; x++)
            {
                _collisionObjects.Add(new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMiniRGB() }));
                _collisionObjects[collidionObjectsCounter].MoveTo(rightWallX + 1 + x, -15);
                collidionObjectsCounter++;

                _collisionObjects.Add(new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMiniRGB() }));
                _collisionObjects[collidionObjectsCounter].MoveTo(leftWallX + 1 + x, -15);
                collidionObjectsCounter++;
            }

            for (int i = 0; i < 50; i += 6)
            {
                _collisionObjects.Add(new BasicBodyOwner(new List<ObjectBody>() { new IndustrialBlock() }));
                _collisionObjects[collidionObjectsCounter].MoveTo(rightWallX, -20 - i);
                collidionObjectsCounter++;

                _collisionObjects.Add(new BasicBodyOwner(new List<ObjectBody>() { new IndustrialBlock() }));
                _collisionObjects[collidionObjectsCounter].MoveTo(leftWallX, -20 - i);
                collidionObjectsCounter++;

                for (int x = -1; x < 2; x++)
                {
                    _collisionObjects.Add(new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMiniRGB() }));
                    _collisionObjects[collidionObjectsCounter].MoveTo(rightWallX + 1 + x, -21 - i);
                    collidionObjectsCounter++;

                    _collisionObjects.Add(new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMiniRGB() }));
                    _collisionObjects[collidionObjectsCounter].MoveTo(leftWallX + 1 + x, -21 - i);
                    collidionObjectsCounter++;
                }
                //
                // TODO
                //
                for (int k = 0; k < 2; k++)
                {
                    var randomObstacle = random.Next(0, 5);

                    switch (randomObstacle)
                    {
                        case 0:

                            for (int x = circleCenter.X - 3; x < (circleCenter.X + 4); x++)
                            {
                                _collisionObjects.Add(new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMiniRGB() }));
                                _collisionObjects[collidionObjectsCounter].MoveTo(x, -21 - i);
                                collidionObjectsCounter++;
                            }
                            break;

                        case 1:

                            for (int x = leftWallX + 3; x < (leftWallX + 12); x++)
                            {
                                _collisionObjects.Add(new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMiniRGB() }));
                                _collisionObjects[collidionObjectsCounter].MoveTo(x, -21 - i);
                                collidionObjectsCounter++;
                            }

                            break;

                        case 2:
                            for (int x = rightWallX; x > (rightWallX - 10); x--)
                            {
                                _collisionObjects.Add(new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMiniRGB() }));
                                _collisionObjects[collidionObjectsCounter].MoveTo(x, -21 - i);
                                collidionObjectsCounter++;
                            }
                            break;
                    }
                }

                _traps.Add(new StraightTrap(new List<ObjectBody>() { new SparkRight() }, 25, StraightTrap.TrapDirection.RIGHT, random.Next(minTrapSpeed - ((i / 6) * 2), 200)));
                _traps[trapsCounter].MoveTo(leftWallX + 3, -21 - i);
                trapsCounter++;

                _traps.Add(new StraightTrap(new List<ObjectBody>() { new SparkLeft() }, 25, StraightTrap.TrapDirection.LEFT, random.Next(minTrapSpeed - ((i / 6) * 2), 200)));
                _traps[trapsCounter].MoveTo(rightWallX, -21 - i);
                trapsCounter++;
            }

            circleCenter = new(GameWorld.Screen.OFFSET.X + 44, GameWorld.Screen.OFFSET.Y - 90);

            for (int i = 0; i < 360; i++)
            {
                int x = circleCenter.X + (int)Math.Round(20 * 2 * Math.Sin(i));
                int y = circleCenter.Y + (int)Math.Round(20 * Math.Cos(i));

                if (!(!(y < (GameWorld.Screen.OFFSET.Y - 90)) && (x < (circleCenter.X + 16) && (x > (circleCenter.X - 16)))))
                {
                    _collisionObjects.AddRange(
                    new List<BasicBodyOwner>()
                    {
                    new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMiniRGB() })
                    }
                    );
                    _collisionObjects[collidionObjectsCounter].MoveTo(x, y);
                    collidionObjectsCounter++;
                }
            }

            for (int i = 0; i < 360; i++)
            {
                int x = circleCenter.X + (int)Math.Round(10 * 2 * Math.Sin(i));
                int y = circleCenter.Y + (int)Math.Round(10 * Math.Cos(i));

                _noneCollisionObjects.AddRange(
               new List<BasicBodyOwner>()
               {
                   new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMini(System.Drawing.Color.DarkGray) })
               }
               );
                _noneCollisionObjects[noneCollisionObjectsCounter].MoveTo(x, y);
                noneCollisionObjectsCounter++;
            }

            _dynamicGameObjects.AddRange(_traps);
        }

        public bool CollisionCheck(Point transferPosition)
        {
            foreach (var obj in _collisionObjects)
            {
                foreach (var body in obj.Represantation)
                {
                    if (body.Position == transferPosition)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public List<List<IRenderableObject>> GetGameObjects()
        {
            Update();

            var returnList = new List<List<IRenderableObject>>();

            var gameObjects = new List<IRenderableObject>();
            gameObjects.AddRange(_traps);
            gameObjects.AddRange(_collisionObjects);
            gameObjects.AddRange(_noneCollisionObjects);
            gameObjects.Add(_gameMainTarget);
            gameObjects.Add(Enemy);

            gameObjects.Add(PlayerOnMap);

            //if (State != SubState.BATTLE)
            //{
            returnList.Add(gameObjects);
            //}

            return returnList;
        }

        public List<List<IRenderableObject>> GetStaticObjects()
        {
            var returnList = new List<List<IRenderableObject>>();

            if (State == SubState.END_SCREEN)
            {
                if (SquadIsDead())
                {
                    // Экран проигрыша

                    var winScreen = new BasicBodyOwner(new List<ObjectBody>() { new LoseScreen() });
                    winScreen.MoveTo(16, 1);
                    returnList.Add(new List<IRenderableObject>() { winScreen });
                }
                else
                {
                    // Экран победы
                    var winScreen = new BasicBodyOwner(new List<ObjectBody>() { new WinScreen() });
                    winScreen.MoveTo(16, 1);
                    returnList.Add(new List<IRenderableObject>() { winScreen });
                }
            }
            else
            {
                var UI = new List<IRenderableObject>()
            {
                _soundLogo,
                _dateTime,
                _squadUIBlock,
                _enemySquadUIBlock,
                GameLogger
            };

                if (ExitingBattleMode || State == SubState.BATTLE)
                {
                    UI.Add(_battleTypeUI);
                }

                if (State == SubState.BATTLE)
                {
                    if (IsExecutingAction)
                    {
                        _selectionUI.MoveTo(SeletionUIPositions[(int)ExecutionTarget]);
                    }
                    else
                    {
                        _selectionUI.MoveTo(SeletionUIPositions[(int)CurrentCharacter]);
                    }
                    UI.Add(_selectionUI);
                }
                else
                {
                    UI.Add(_gameWorldUI);
                }

                if (ExitingBattleMode)
                {
                    UI.Add(_actionsPanelSmall);
                }
                else if (EnteringBattleMode)
                {
                    UI.Add(_actionsPanelSmall);
                }
                else if (State == SubState.MOVING)
                {
                    UI.Add(_actionsPanelUIBlock);
                }
                else if (State == SubState.BATTLE)
                {
                    UI.Add(_actionsPanelSmall);
                }

                if (State == SubState.BATTLE && !ExitingBattleMode && !EnteringBattleMode)
                {
                    UI.Add(_battleUIBlock);
                }

                if (LeftCharacterUI != null)
                {
                    UI.Add(LeftCharacterUI);
                }
                if (MidCharacterUI != null)
                {
                    UI.Add(MidCharacterUI);
                }
                if (RightCharacterUI != null)
                {
                    UI.Add(RightCharacterUI);
                }

                var characters = new List<IRenderableObject>() { };
                if (LeftCharacterBody != null)
                {
                    characters.Add(LeftCharacterBody);
                }
                if (MidCharacterBody != null)
                {
                    characters.Add(MidCharacterBody);
                }
                if (RightCharacterBody != null)
                {
                    characters.Add(RightCharacterBody);
                }

                if (State == SubState.BATTLE)
                {
                    if (_currentOpponent != null)
                    {
                        if (_currentOpponent.LeftEnemy != null)
                        {
                            characters.Add(_currentOpponent.LeftEnemy);
                        }
                        if (_currentOpponent.MidEnemy != null)
                        {
                            characters.Add(_currentOpponent.MidEnemy);
                        }
                        if (_currentOpponent.RightEnemy != null)
                        {
                            characters.Add(_currentOpponent.RightEnemy);
                        }
                        if (LeftEnemyUI != null)
                        {
                            UI.Add(LeftEnemyUI);
                        }
                        if (MidEnemyUI != null)
                        {
                            UI.Add(MidEnemyUI);
                        }
                        if (RightEnemyUI != null)
                        {
                            UI.Add(RightEnemyUI);
                        }

                        if (CurrentAction == InBattleAction.LEFT_ACTION)
                        {
                            _battleActionSelectionUI.MoveTo(2, 27);
                        }
                        else
                        {
                            _battleActionSelectionUI.MoveTo(31, 27);
                        }

                        _leftBattleAction.SetAction(SelectedGameCharacter.LeftAction);
                        _rightBattleAction.SetAction(SelectedGameCharacter.RightAction);

                        UI.Add(_leftBattleAction);
                        UI.Add(_rightBattleAction);
                        UI.Add(_battleActionSelectionUI);
                    }
                }

                returnList.Add(characters);

                returnList.Add(UI);
            }

            return returnList;
        }

        public void InitializeScene()
        {
            LeftCharacter = GameWorld.CharecterCreation.LeftCharacter;
            MidCharacter = GameWorld.CharecterCreation.MidCharacter;
            RightCharacter = GameWorld.CharecterCreation.RightCharacter;
            if (RightCharacter != null)
            {
                RightCharacterBody = new BasicBodyOwner(
                        new List<ObjectBody>() { CharacterCreationScene.CharacterBodyByType(GameWorld.CharecterCreation.RightCharacter.Type,
                        GameWorld.CharecterCreation.RightCharacter.CharacterColor) }
                        );
                RightCharacterBody.MoveTo(3, 17);

                RightCharacterUI = new BasicBodyOwner(new List<ObjectBody>() { new CharacterUI(RightCharacter) });
                RightCharacterUI.MoveTo(2, 21);

                SelectedGameCharacter = RightCharacter;
            }
            if (MidCharacter != null)
            {
                MidCharacterBody = new BasicBodyOwner(
                        new List<ObjectBody>() { CharacterCreationScene.CharacterBodyByType(GameWorld.CharecterCreation.MidCharacter.Type,
                        GameWorld.CharecterCreation.MidCharacter.CharacterColor) }
                        );
                MidCharacterBody.MoveTo(3, 10);

                MidCharacterUI = new BasicBodyOwner(new List<ObjectBody>() { new CharacterUI(MidCharacter) });
                MidCharacterUI.MoveTo(2, 14);

                SelectedGameCharacter = MidCharacter;
            }
            if (LeftCharacter != null)
            {
                LeftCharacterBody = new BasicBodyOwner(
                        new List<ObjectBody>() { CharacterCreationScene.CharacterBodyByType(GameWorld.CharecterCreation.LeftCharacter.Type,
                        GameWorld.CharecterCreation.LeftCharacter.CharacterColor) }
                        );
                LeftCharacterBody.MoveTo(3, 3);

                LeftCharacterUI = new BasicBodyOwner(new List<ObjectBody>() { new CharacterUI(LeftCharacter) });
                LeftCharacterUI.MoveTo(2, 7);

                SelectedGameCharacter = LeftCharacter;
            }

            _dateTime = new BasicBodyOwner(new List<ObjectBody>() { new DateTimeBody() });
            _soundLogo = new BasicBodyOwner(new List<ObjectBody>() { new SoundLogoBody() });
            _soundLogo.MoveTo(120, 0);

            _squadUIBlock = new BasicBodyOwner(new List<ObjectBody>() { new SquadUIBlock() });
            _squadUIBlock.MoveTo(0, 1);
            _enemySquadUIBlock = new BasicBodyOwner(new List<ObjectBody>() { new EnemySquadUIBlock() });
            _enemySquadUIBlock.MoveTo(107, 1);
            _gameWorldUI = new BasicBodyOwner(new List<ObjectBody>() { new GameWorldUI() });
            _gameWorldUI.MoveTo(16, 1);

            _battleTypeUI = new BasicBodyOwner(new List<ObjectBody>() { new BattleTypeUI() });
            _battleTypeUI.MoveTo(16, 1);

            _battleActionSelectionUI = new BasicBodyOwner(new List<ObjectBody>() { new BattleActionSelection() });

            _leftBattleAction = new BattleActionButton();
            _leftBattleAction.TargetPosition = new Point(2, 27);
            _rightBattleAction = new BattleActionButton();
            _rightBattleAction.TargetPosition = new Point(31, 27);

            _selectionUI = new BasicBodyOwner(new List<ObjectBody>() { new SelectedCharacterUI() });

            _actionsPanelUIBlock = new BasicBodyOwner(new List<ObjectBody>() { new ActionUIBlock() });
            _actionsPanelUIBlock.MoveTo(0, 26);

            _actionsPanelSmall = new BasicBodyOwner(new List<ObjectBody>() { new ActionUIBlockSmall() });
            _actionsPanelSmall.MoveTo(0, 26);

            _battleUIBlock = new BasicBodyOwner(new List<ObjectBody>() { new BattleActionBlock() });
            _battleUIBlock.MoveTo(0, 26);

            if (PlayerOnMap == null)
            {
                // Создание мира и фишки игрока

                PlayerOnMap = new BasicBodyOwner(new List<ObjectBody>() { new MiniCharacterIcon() });
                PlayerOnMap.MoveTo(60, 13);
                BuildGameWorld();
                GameLogger = new GlobalMapLogger();
                GameLogger.TargetPosition = new Point(4, 27);
                Enemy = new EnemySquad(new List<ObjectBody>() { new ObstacleWallMini() }, new Point(GameWorld.Screen.OFFSET.X + 44, GameWorld.Screen.OFFSET.Y - 90), 15, "Разбойники", 250,
                    new Outlaw(new List<ObjectBody>() { new OutlawBody(System.Drawing.Color.FromArgb(255, 100, 250, 100)) }, "Головорез", 10, System.Drawing.Color.Red),
                    new Outlaw(new List<ObjectBody>() { new OutlawBody(System.Drawing.Color.FromArgb(255, 250, 100, 100)) }, "Бандит", 10, System.Drawing.Color.Blue),
                    new Outlaw(new List<ObjectBody>() { new OutlawBody(System.Drawing.Color.FromArgb(255, 100, 100, 250)) }, "Преступник", 10, System.Drawing.Color.Yellow)
                    );
                Enemy.MoveTo(GameWorld.Screen.OFFSET.X + 44, GameWorld.Screen.OFFSET.Y - 90);
                //Enemy.MoveTo(GameWorld.Screen.OFFSET.X + 44, GameWorld.Screen.OFFSET.Y);
                _dynamicGameObjects.Add(Enemy);

                _gameMainTarget = new BasicBodyOwner(new List<ObjectBody>() { new ObstacleWallMini(System.Drawing.Color.Gold) });
                _gameMainTarget.MoveTo(GameWorld.Screen.OFFSET.X + 44, GameWorld.Screen.OFFSET.Y - 98);

                GameLogger.AddMessage($"Найдите золотой аретфакт чтобы выиграть!", GlobalMapLogger.MessageType.GOOD);
            }
        }

        public bool MovePlayer(Point movement)
        {
            foreach (var point in GameWorld.GlobalMap.PlayerOnMap.Represantation)
            {
                if (!CollisionCheck(new Point(point.Position.X + movement.X, point.Position.Y + movement.Y)))
                {
                    return false;
                }
            }
            PlayerOnMap.Move(movement.X, movement.Y);
            return true;
        }

        public void StartBattle(EnemySquad enemySquad)
        {
            if (State != SubState.BATTLE && !EnteringBattleMode && !GameEnded)
            {
                GameWorld.GlobalMap.GameLogger.AddMessage($"{enemySquad.Name} нападает на игрока!", GlobalMapLogger.MessageType.BAD);

                if (enemySquad.LeftEnemy != null)
                {
                    enemySquad.LeftEnemy.MoveTo(111, 3);
                    LeftEnemyUI = new BasicBodyOwner(new List<ObjectBody>() { new CharacterUI(enemySquad.LeftEnemy) });
                    LeftEnemyUI.MoveTo(109, 7);
                }
                if (enemySquad.MidEnemy != null)
                {
                    enemySquad.MidEnemy.MoveTo(111, 10);
                    MidEnemyUI = new BasicBodyOwner(new List<ObjectBody>() { new CharacterUI(enemySquad.MidEnemy) });
                    MidEnemyUI.MoveTo(109, 14);
                }
                if (enemySquad.RightEnemy != null)
                {
                    enemySquad.RightEnemy.MoveTo(111, 17);
                    RightEnemyUI = new BasicBodyOwner(new List<ObjectBody>() { new CharacterUI(enemySquad.RightEnemy) });
                    RightEnemyUI.MoveTo(109, 21);
                }

                _currentOpponent = enemySquad;

                SwitchBattleMode(true);
            }
        }

        public void ActionExecuted()
        {
            IsPlayersMove = false;
        }

        private bool EnenmySquadDead()
        {
            return (_currentOpponent.LeftEnemy == null && _currentOpponent.RightEnemy == null && _currentOpponent.MidEnemy == null);
        }

        public void ConductBattle()
        {
            if (State == SubState.BATTLE && _currentOpponent != null)
            {
                if (_currentOpponent.LeftEnemy != null && _currentOpponent.LeftEnemy.HP <= 0)
                {
                    _currentOpponent.LeftEnemy = null;
                    LeftEnemyUI = null;

                    if (!EnenmySquadDead())
                    {
                        SwitchExecutionTarget(TargetSelectionSwitch.DOWN);
                    }
                }
                if (_currentOpponent.MidEnemy != null && _currentOpponent.MidEnemy.HP <= 0)
                {
                    _currentOpponent.MidEnemy = null;
                    MidEnemyUI = null;

                    if (!EnenmySquadDead())
                    {
                        SwitchExecutionTarget(TargetSelectionSwitch.DOWN);
                    }
                }
                if (_currentOpponent.RightEnemy != null && _currentOpponent.RightEnemy.HP <= 0)
                {
                    _currentOpponent.RightEnemy = null;
                    RightEnemyUI = null;

                    if (!EnenmySquadDead())
                    {
                        SwitchExecutionTarget(TargetSelectionSwitch.DOWN);
                    }
                }

                if (EnenmySquadDead())
                {
                    GameLogger.AddMessage($"Вы победели отряд {_currentOpponent.Name}", GlobalMapLogger.MessageType.GOOD);
                    _currentOpponent.Die();
                    _currentOpponent = null;
                    SwitchBattleMode(false);
                    IsPlayersMove = true;
                }
                else if ((DateTime.Now - _lastTimePlayerMoved).TotalSeconds >= 2)

                {
                    Random random = new Random();

                    if (!IsPlayersMove)
                    {
                        IDiableCharacter randomEnemy;

                        do
                        {
                            var randomEnemyID = random.Next(0, 3);
                            switch (randomEnemyID)
                            {
                                case 0:
                                    randomEnemy = _currentOpponent.LeftEnemy;
                                    break;

                                case 1:
                                    randomEnemy = _currentOpponent.MidEnemy;
                                    break;

                                default:
                                    randomEnemy = _currentOpponent.RightEnemy;
                                    break;
                            }
                        } while (randomEnemy == null);

                        IDiableCharacter character;

                        do
                        {
                            var randomCharacterID = random.Next(0, 3);
                            switch (randomCharacterID)
                            {
                                case 0:
                                    character = LeftCharacter;
                                    break;

                                case 1:
                                    character = MidCharacter;
                                    break;

                                default:
                                    character = RightCharacter;
                                    break;
                            }
                        } while (character == null);

                        int damage = 3;

                        if (GameWorld.SelectedDifficulty == GameDifficulty.Medium)
                        {
                            damage = 4;
                        }
                        else
                        {
                            damage = 5;
                        }

                        var res = character.TakeDamage(damage);

                        GameSoundPlayer.PlayDamageSound();

                        GameLogger.AddMessage($"{Enemy.Name} наносит {damage} урона {character.Name}", GlobalMapLogger.MessageType.BAD);

                        if (res)
                        {
                            GameLogger.AddMessage($"Персонаж {character.Name} погибает...", GlobalMapLogger.MessageType.BAD);

                            if (SquadIsDead() && !GameEnded)
                            {
                                GameSoundPlayer.PlayGameOverSound();
                                State = SubState.END_SCREEN;
                                _busy = true;
                                GameEnded = true;
                            }
                        }

                        IsPlayersMove = true;
                    }
                }
            }
        }

        public void ExecuteAction()
        {
            BattleAction currentAction = (CurrentAction == InBattleAction.LEFT_ACTION) ? SelectedGameCharacter.LeftAction : SelectedGameCharacter.RightAction;

            IDiableCharacter target;

            switch (ExecutionTarget)
            {
                case InBattleExecutionSelection.LEFT_UP:
                    target = LeftCharacter;
                    break;

                case InBattleExecutionSelection.LEFT_MID:
                    target = MidCharacter;
                    break;

                case InBattleExecutionSelection.LEFT_DOWN:
                    target = RightCharacter;
                    break;

                case InBattleExecutionSelection.RIGHT_UP:
                    target = _currentOpponent.LeftEnemy;
                    break;

                case InBattleExecutionSelection.RIGHT_MID:
                    target = _currentOpponent.MidEnemy;
                    break;
                //case InBattleExecutionSelection.RIGHT_DOWN:
                default:
                    target = _currentOpponent.RightEnemy;
                    break;
            }

            if (GameWorld.EnergyLeft >= currentAction.Cost)
            {
                if (currentAction.Damage > 0)
                {
                    GameSoundPlayer.PlayDamageSound();
                    GameLogger.AddMessage($"Игрок {SelectedGameCharacter.Name} наносит {currentAction.Damage} урона {target.Name}", GlobalMapLogger.MessageType.GOOD);
                }
                else if (currentAction.Damage < 0)
                {
                    GameSoundPlayer.PlayRegenarationSound();
                    GameLogger.AddMessage($"Игрок {SelectedGameCharacter.Name} восстанавливает {-currentAction.Damage} ХП {target.Name}", GlobalMapLogger.MessageType.GOOD);
                }
                else if (currentAction.Cost < 0)
                {
                    GameSoundPlayer.PlayRegenarationSound();
                    GameLogger.AddMessage($"Игрок {SelectedGameCharacter.Name} восстанавливает {-currentAction.Cost} ЭН", GlobalMapLogger.MessageType.GOOD);
                }

                target.TakeDamage(currentAction.Damage);
                GameWorld.EnergyLeft -= currentAction.Cost;
                if (GameWorld.EnergyLeft > GameWorld.MaxEnergy)
                {
                    GameWorld.EnergyLeft = GameWorld.MaxEnergy;
                }

                IsExecutingAction = false;
                IsPlayersMove = false;
                _lastTimePlayerMoved = DateTime.Now;
            }
            else
            {
                GameLogger.AddMessage($"Недостаточно энергии", GlobalMapLogger.MessageType.COMMON);
            }
        }

        public void SwitchExecutionTarget(TargetSelectionSwitch direction)
        {
            InBattleExecutionSelection target;

            bool isLeft = (ExecutionTarget == InBattleExecutionSelection.LEFT_DOWN || ExecutionTarget == InBattleExecutionSelection.LEFT_MID || ExecutionTarget == InBattleExecutionSelection.LEFT_UP);
            bool isDown = (direction == TargetSelectionSwitch.DOWN);

            if (direction == TargetSelectionSwitch.RIGHT)
            {
                if (isLeft)
                {
                    if (_currentOpponent.LeftEnemy != null)
                    {
                        ExecutionTarget = InBattleExecutionSelection.RIGHT_UP;
                    }
                    else if (_currentOpponent.MidEnemy != null)
                    {
                        ExecutionTarget = InBattleExecutionSelection.RIGHT_MID;
                    }
                    else if (_currentOpponent.RightEnemy != null)
                    {
                        ExecutionTarget = InBattleExecutionSelection.RIGHT_DOWN;
                    }
                }
            }
            else if (direction == TargetSelectionSwitch.LEFT)
            {
                if (!isLeft)
                {
                    if (LeftCharacter != null)
                    {
                        ExecutionTarget = InBattleExecutionSelection.LEFT_UP;
                    }
                    else if (MidCharacter != null)
                    {
                        ExecutionTarget = InBattleExecutionSelection.LEFT_MID;
                    }
                    else if (RightCharacter != null)
                    {
                        ExecutionTarget = InBattleExecutionSelection.LEFT_DOWN;
                    }
                }
            }
            else
            {
                InBattleExecutionSelection suggestedCharacter = ExecutionTarget;
                var next = isDown ? +1 : -1;

                if (isLeft)
                {
                    if ((int)(suggestedCharacter + next) < 0)
                    {
                        suggestedCharacter = (InBattleExecutionSelection)(Math.Abs((int)suggestedCharacter + next + 3) % 3);
                    }
                    else
                    {
                        suggestedCharacter = (InBattleExecutionSelection)(Math.Abs((int)suggestedCharacter + next) % 3);
                    }

                    if (suggestedCharacter == InBattleExecutionSelection.LEFT_UP && LeftCharacter == null ||
                        suggestedCharacter == InBattleExecutionSelection.LEFT_MID && MidCharacter == null ||
                        suggestedCharacter == InBattleExecutionSelection.LEFT_DOWN && RightCharacter == null)
                    {
                        ExecutionTarget += next;
                        if (isDown)
                        {
                            SwitchExecutionTarget(TargetSelectionSwitch.DOWN);
                        }
                        else
                        {
                            SwitchExecutionTarget(TargetSelectionSwitch.UP);
                        }
                    }
                    else
                    {
                        ExecutionTarget = suggestedCharacter;
                    }
                }
                else
                {
                    if ((int)(suggestedCharacter + next) < 0)
                    {
                        suggestedCharacter = (InBattleExecutionSelection)(Math.Abs((int)suggestedCharacter + next + 3) % 3 + 3);
                    }
                    else
                    {
                        suggestedCharacter = (InBattleExecutionSelection)(Math.Abs((int)suggestedCharacter + next) % 3 + 3);
                    }

                    if (suggestedCharacter == InBattleExecutionSelection.RIGHT_UP && _currentOpponent.LeftEnemy == null ||
                        suggestedCharacter == InBattleExecutionSelection.RIGHT_MID && _currentOpponent.MidEnemy == null ||
                        suggestedCharacter == InBattleExecutionSelection.RIGHT_DOWN && _currentOpponent.RightEnemy == null)
                    {
                        ExecutionTarget += next;
                        if (isDown)
                        {
                            SwitchExecutionTarget(TargetSelectionSwitch.DOWN);
                        }
                        else
                        {
                            SwitchExecutionTarget(TargetSelectionSwitch.UP);
                        }
                    }
                    else
                    {
                        ExecutionTarget = suggestedCharacter;
                    }
                }
            }
        }

        public void SwitchSelectedActions()
        {
            GameSoundPlayer.PlayButtonSound();
            if (CurrentAction == InBattleAction.LEFT_ACTION)
            {
                CurrentAction = InBattleAction.RIGHT_ACTION;
            }
            else
            {
                CurrentAction = InBattleAction.LEFT_ACTION;
            }
        }

        public void SwitchActionExecution(bool value)
        {
            IsExecutingAction = value;
        }

        public void SwitchSelectedCharacter(int next)
        {
            if ((State == SubState.BATTLE || EnteringBattleMode) && !SquadIsDead())
            {
                InBattleCharacterSelection suggestedCharacter = CurrentCharacter;

                if ((int)(suggestedCharacter + next) < 0)
                {
                    suggestedCharacter = (InBattleCharacterSelection)(Math.Abs((int)suggestedCharacter + next + Enum.GetNames(typeof(InBattleCharacterSelection)).Length) % Enum.GetNames(typeof(InBattleCharacterSelection)).Length);
                }
                else
                {
                    suggestedCharacter = (InBattleCharacterSelection)(Math.Abs((int)suggestedCharacter + next) % Enum.GetNames(typeof(InBattleCharacterSelection)).Length);
                }

                if (suggestedCharacter == InBattleCharacterSelection.LEFT_CHARACTER && LeftCharacter == null ||
                    suggestedCharacter == InBattleCharacterSelection.MID_CHARACTER && MidCharacter == null ||
                    suggestedCharacter == InBattleCharacterSelection.RIGHT_CHARACTER && RightCharacter == null)
                {
                    SwitchSelectedCharacter(next + 1 * Math.Sign(next));
                }
                else
                {
                    CurrentCharacter = suggestedCharacter;
                    switch (CurrentCharacter)
                    {
                        case InBattleCharacterSelection.LEFT_CHARACTER:
                            if (LeftCharacter != null)
                            {
                                SelectedGameCharacter = LeftCharacter;
                            }

                            break;

                        case InBattleCharacterSelection.MID_CHARACTER:
                            if (MidCharacter != null)
                            {
                                SelectedGameCharacter = MidCharacter;
                            }

                            break;

                        case InBattleCharacterSelection.RIGHT_CHARACTER:
                            if (RightCharacter != null)
                            {
                                SelectedGameCharacter = RightCharacter;
                            }

                            break;
                    }
                }
            }
        }

        public void SwitchBattleMode(bool value)
        {
            _busy = true;
            _animationStart = DateTime.Now;
            if (value)
            {
                EnteringBattleMode = true;

                //switch (CurrentCharacter)
                //{
                //    case InBattleCharacterSelection.LEFT_CHARACTER:
                //        //SelectedGameCharacter = LeftCharacter;
                //        break;
                //
                //    case InBattleCharacterSelection.MID_CHARACTER:
                //        //SelectedGameCharacter = MidCharacter;
                //        break;
                //
                //    case InBattleCharacterSelection.RIGHT_CHARACTER:
                //        //SelectedGameCharacter = RightCharacter;
                //        break;
                //}

                SwitchSelectedCharacter(1);
            }
            else
            {
                ExitingBattleMode = true;
            }
        }

        public bool GameEnded;

        private void GameGoalCheck()
        {
            if (PlayerOnMap != null && !GameEnded)
            {
                foreach (var p in PlayerOnMap.Represantation)
                {
                    if (p.Position == _gameMainTarget.Represantation[0].Position)
                    {
                        State = SubState.END_SCREEN;
                        _busy = true;
                        GameWorld.GameEndTime = DateTime.Now;
                        GameSoundPlayer.PlayGameOverSound();
                        GameEnded = true;
                    }
                }
            }
        }

        public void Update()
        {
            GameGoalCheck();
            ConductBattle();

            foreach (var gameObject in _dynamicGameObjects)
            {
                gameObject.Update();
            }
            if (GameEnded)
            {
                if ((DateTime.Now - GameWorld.GameEndTime).TotalSeconds > 2)
                {
                    _busy = false;
                }
                else
                {
                    _busy = true;
                }
            }

            if (LeftCharacter != null && LeftCharacter.HP <= 0)
            {
                //GameLogger.AddMessage($"Персонаж {LeftCharacter.Name} погибает...", GlobalMapLogger.MessageType.BAD);
                LeftCharacter = null;
                LeftCharacterBody = null;
                LeftCharacterUI = null;
                if (!SquadIsDead())
                {
                    SwitchSelectedCharacter(1);
                }
            }
            if (MidCharacter != null && MidCharacter.HP <= 0)
            {
                //GameLogger.AddMessage($"Персонаж {MidCharacter.Name} погибает...", GlobalMapLogger.MessageType.BAD);
                MidCharacter = null;
                MidCharacterBody = null;
                MidCharacterUI = null;
                if (!SquadIsDead())
                {
                    SwitchSelectedCharacter(1);
                }
            }
            if (RightCharacter != null && RightCharacter.HP <= 0)
            {
                //GameLogger.AddMessage($"Персонаж {RightCharacter.Name} погибает...", GlobalMapLogger.MessageType.BAD);
                RightCharacter = null;
                RightCharacterBody = null;
                RightCharacterUI = null;
                if (!SquadIsDead())
                {
                    SwitchSelectedCharacter(1);
                }
            }
            IsPlayerInTrap = false;

            if (SquadIsDead() && !GameEnded)
            {
                State = SubState.END_SCREEN;
                GameSoundPlayer.PlayGameOverSound();
                if (!_squadAlreadyDead)
                {
                    _gameOverTime = DateTime.Now;
                    GameWorld.GameEndTime = DateTime.Now;
                }
                GameEnded = true;
                _squadAlreadyDead = true;
            }
            else
            {
                var currentTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

                foreach (var gameObject in _traps)
                {
                    foreach (var bodyPoint in gameObject.Represantation)
                    {
                        foreach (var playerBodyPoint in this.PlayerOnMap.Represantation)
                        {
                            if (bodyPoint.Position == playerBodyPoint.Position)
                            {
                                // Урон от ловушки

                                IsPlayerInTrap = true;

                                if (currentTime - _lastHitByTrap > 1000)
                                {
                                    var damage = 1;
                                    if (GameWorld.SelectedDifficulty == GameDifficulty.Medium)
                                    {
                                        damage = 2;
                                    }
                                    else if (GameWorld.SelectedDifficulty == GameDifficulty.Hard)
                                    {
                                        damage = 3;
                                    }

                                    _lastHitByTrap = currentTime;
                                    this.GameLogger.AddMessage($"Отряду нанесён урон ({damage}) ловушкой", GlobalMapLogger.MessageType.BAD);

                                    GameSoundPlayer.PlayDamageSound();

                                    if (LeftCharacter != null)
                                    {
                                        var leftDead = LeftCharacter.TakeDamage(damage);
                                    }
                                    if (MidCharacter != null)
                                    {
                                        var midDead = MidCharacter.TakeDamage(damage);
                                    }
                                    if (RightCharacter != null)
                                    {
                                        var rightDead = RightCharacter.TakeDamage(damage);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (_busy)
            {
                if (EnteringBattleMode && !SquadIsDead())
                {
                    _enteringCount = (int)((DateTime.Now - _animationStart).TotalMilliseconds) / 15 + 4;

                    if (_enteringCount >= 62 + 4)
                    {
                        EnteringBattleMode = false;
                        _busy = false;
                        GameLogger.TargetPosition = new Point(62 + 4, GameLogger.TargetPosition.Y);
                        _actionsPanelSmall.MoveTo(62, 26);
                        State = SubState.BATTLE;
                    }
                    else
                    {
                        _actionsPanelSmall.MoveTo(_enteringCount - 4, 26);
                        GameLogger.TargetPosition = new Point(_enteringCount, GameLogger.TargetPosition.Y);
                    }
                }
                else if (ExitingBattleMode && !SquadIsDead())
                {
                    _exitingCount = (int)((DateTime.Now - _animationStart).TotalMilliseconds) / 15;

                    if (_exitingCount >= 62)
                    {
                        ExitingBattleMode = false;
                        _busy = false;
                        GameLogger.TargetPosition = new Point(4, GameLogger.TargetPosition.Y);
                        _actionsPanelSmall.MoveTo(0, 26);
                        State = SubState.MOVING;
                    }
                    else
                    {
                        GameLogger.TargetPosition = new Point(62 + 4 - _exitingCount, GameLogger.TargetPosition.Y);
                        _actionsPanelSmall.MoveTo(62 - _exitingCount, 26);
                    }
                }
            }
        }

        private bool SquadIsDead()
        {
            return (LeftCharacter == null && MidCharacter == null && RightCharacter == null);
        }
    }
}