﻿using TheGallowsSurvival.GameObjects;
using TheGallowsSurvival.GameObjects.Bodies;
using TheGallowsSurvival.GameObjects.Bodies.General;
using TheGallowsSurvival.GameObjects.Bodies.MainMenu;
using TheGallowsSurvival.GameScene;

namespace TheGallowsSurvival.GameScenes
{
    public enum GameDifficulty
    { Easy, Medium, Hard }

    internal class MainMenuScene : IGameScene
    {
        public enum ButtonType
        { Play, Sound, Exit }

        private ButtonType _currentButtonType;

        public enum ButtonInteractions
        { Default, Up, Down }

        public bool SelectingDifficulty;

        private GameDifficulty _selectedDifficulty;

        public GameDifficulty SelectedDifficulty
        {
            get { return _selectedDifficulty; }
            private set
            {
                if ((int)value >= Enum.GetNames(typeof(GameDifficulty)).Length)
                {
                    value = 0;
                }
                else if ((int)value < 0)
                {
                    value = (GameDifficulty)Enum.GetNames(typeof(GameDifficulty)).Length - 1;
                }
                _selectedDifficulty = value;
            }
        }

        private bool _isBusy;
        private bool _enteringDifficulty;
        private bool _exitingDifficulty;

        public bool EnsuringExit;

        private DateTime _animationStart;
        private DateTime _soundButtonEnter;

        public bool IsBusy
        {
            get { return _isBusy; }
        }

        private BasicBodyOwner _playButton;

        private BasicBodyOwner _soundButton;

        private BasicBodyOwner _exitButton;

        private BasicBodyOwner _soundLogo;

        private BasicBodyOwner _mainLogo;

        private BasicBodyOwner _dateTime;

        private BasicBodyOwner _easyDifficultyButton;
        private BasicBodyOwner _mediumDifficultyButton;
        private BasicBodyOwner _hardDifficultyButton;

        public void InitializeButtons()
        {
            _isBusy = false;
            _enteringCount = 0;
            _enteringDifficulty = false;
            _exitingCount = 0;
            _exitingDifficulty = false;
            SelectingDifficulty = false;

            _playButton = new BasicBodyOwner(new List<ObjectBody>() { new MenuPlayButtonBodyUnselected(), new MenuPlayButtonBodySelected(), new MenuPlayButtonMini() });
            _playButton.MoveTo(42, 14);
            _soundButton = new BasicBodyOwner(new List<ObjectBody>() { new MenuSoundButtonBodyUnselected(), new MenuSoundButtonBodySelected() });
            _soundButton.MoveTo(42, 20);
            _exitButton = new BasicBodyOwner(new List<ObjectBody>() { new MenuExitButtonBodyUnselected(), new MenuExitButtonBodySelected(), new MenuExitButtonBodySelectedEnsure() });
            _exitButton.MoveTo(42, 26);

            _mainLogo = new BasicBodyOwner(new List<ObjectBody>() { new BigMainLogoBody() });
            _mainLogo.MoveTo(33 / 2, 1);
            _dateTime = new BasicBodyOwner(new List<ObjectBody>() { new DateTimeBody() });
            _soundLogo = new BasicBodyOwner(new List<ObjectBody>() { new SoundLogoBody() });
            _soundLogo.MoveTo(120, 0);

            _easyDifficultyButton = new BasicBodyOwner(new List<ObjectBody>() { new EasyDifficultyButtonBodyUnselected(), new EasyDifficultyButtonBodySelected() });
            _easyDifficultyButton.MoveTo(41, 14);

            _mediumDifficultyButton = new BasicBodyOwner(new List<ObjectBody>() { new MediumDifficultyButtonBodyUnselected(), new MediumDifficultyButtonBodySelected() });
            _mediumDifficultyButton.MoveTo(64, 14);

            _hardDifficultyButton = new BasicBodyOwner(new List<ObjectBody>() { new HardDifficultyButtonBodyUnselected(), new HardDifficultyButtonBodySelected() });
            _hardDifficultyButton.MoveTo(87, 14);

            HighlightButton();
        }

        public void ChangeButton(ButtonInteractions interactions)
        {


            if (SelectingDifficulty)
            {
                switch (interactions)
                {
                    case ButtonInteractions.Default:
                        {
                            SelectedDifficulty = 0;
                        }
                        break;

                    case ButtonInteractions.Up:
                        {
                            SelectedDifficulty--;
                        }
                        break;

                    case ButtonInteractions.Down:
                        {
                            SelectedDifficulty++;
                        }
                        break;
                }
            }
            else
            {
                switch (interactions)
                {
                    case ButtonInteractions.Default:
                        {
                            CurrentButtonType = 0;
                        }
                        break;

                    case ButtonInteractions.Up:
                        {
                            CurrentButtonType--;
                        }
                        break;

                    case ButtonInteractions.Down:
                        {
                            CurrentButtonType++;
                        }
                        break;
                }
            }

            HighlightButton();
        }

        public void SwitchEnsuringExit(bool value)
        {
            EnsuringExit = value;
            HighlightButton();
        }

        public void SwitchDifficultySelection(bool value)
        {
            if (value)
            {
                SelectingDifficulty = true;

                StartEnterDifficultyAnimation();
            }
            else
            {
                SelectingDifficulty = false;
                StartExitDifficultyAnimation();
            }
            HighlightButton();
        }

        private void StartEnterDifficultyAnimation()
        {
            _isBusy = true;
            _enteringDifficulty = true;
            _animationStart = DateTime.Now;
        }

        private void StartExitDifficultyAnimation()
        {
            _isBusy = true;
            _exitingDifficulty = true;
            _animationStart = DateTime.Now;
        }

        private int _enteringCount = 0;
        private int _exitingCount = 0;

        public void Update()
        {
            if (_isBusy)
            {
                if (_enteringDifficulty)
                {
                    _enteringCount = (int)((DateTime.Now - _animationStart).TotalMilliseconds) / 10;

                    if (_enteringCount < (42 - 16))
                    {
                        _playButton.MoveTo(42 - _enteringCount, 14);
                    }
                    else
                    {
                        _playButton.MoveTo(16, 14);
                        _isBusy = false;
                        _enteringCount = 0;
                        _enteringDifficulty = false;
                    }
                }
                else if (_exitingDifficulty)
                {
                    _exitingCount = (int)(DateTime.Now - _animationStart).TotalMilliseconds / 10;

                    if (_exitingCount < (42 - 16))
                    {
                        _playButton.MoveTo(16 + _exitingCount, 14);
                    }
                    else
                    {
                        _playButton.MoveTo(42, 14);
                        _isBusy = false;
                        _exitingCount = 0;
                        _exitingDifficulty = false;
                    }
                }
            }
        }

        private void HighlightButton()
        {
            _playButton.State = 0;
            _soundButton.State = 0;
            _exitButton.State = 0;
            _easyDifficultyButton.State = 0;
            _mediumDifficultyButton.State = 0;
            _hardDifficultyButton.State = 0;

            if (EnsuringExit)
            {
                _exitButton.State = 2;
            }
            else if (SelectingDifficulty)
            {
                _playButton.State = 2;

                switch (SelectedDifficulty)
                {
                    case GameDifficulty.Easy:
                        {
                            _easyDifficultyButton.State = 1;
                        }
                        break;

                    case GameDifficulty.Medium:
                        {
                            _mediumDifficultyButton.State = 1;
                        }
                        break;

                    case GameDifficulty.Hard:
                        {
                            _hardDifficultyButton.State = 1;
                        }
                        break;
                }
            }
            else
            {
                switch (CurrentButtonType)
                {
                    case ButtonType.Play:
                        {
                            _playButton.State = 1;
                        }
                        break;

                    case ButtonType.Sound:
                        {
                            _soundButton.State = 1;
                        }
                        break;

                    case ButtonType.Exit:
                        {
                            _exitButton.State = 1;
                        }
                        break;
                }
            }
        }

        public ButtonType CurrentButtonType
        {
            get { return _currentButtonType; }
            private set
            {
                if ((int)value >= Enum.GetNames(typeof(ButtonType)).Length)
                {
                    value = 0;
                }
                else if ((int)value < 0)
                {
                    value = (ButtonType)Enum.GetNames(typeof(ButtonType)).Length - 1;
                }
                _currentButtonType = value;
            }
        }

        public List<List<IRenderableObject>> GetStaticObjects()
        {
            var returnList = new List<List<IRenderableObject>>();

            var buttons = new List<IRenderableObject>()
            {
                _playButton, _soundButton, _exitButton, _soundLogo, _mainLogo, _dateTime
            };

            if (!IsBusy && SelectingDifficulty)
            {
                buttons.Add(_easyDifficultyButton);
                buttons.Add(_mediumDifficultyButton);
                buttons.Add(_hardDifficultyButton);
            }

            returnList.Add(buttons);

            return returnList;
        }

        public List<List<IRenderableObject>> GetGameObjects()
        {
            return new List<List<IRenderableObject>>();
        }
    }
}