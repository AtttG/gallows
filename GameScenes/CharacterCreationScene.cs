﻿using System.Drawing;
using TheGallowsSurvival.GameObjects;
using TheGallowsSurvival.GameObjects.Bodies;
using TheGallowsSurvival.GameObjects.Bodies.CharacterCreation;
using TheGallowsSurvival.GameObjects.Bodies.Characters;
using TheGallowsSurvival.GameObjects.Bodies.General;
using TheGallowsSurvival.Represantation;

namespace TheGallowsSurvival.GameScene
{
    internal class CharacterCreationScene : IGameScene
    {
        public enum ButtonType
        { CharecterLeft, CharecterMid, CharecterRight, Play }

        public bool IsBusy
        {
            get
            {
                foreach (var gameObject in _dynamicGameObjects)
                {
                    if (gameObject.IsBusy())
                    {
                        return true;
                    }
                }
                return false;
            }
        }

        public enum ButtonInteractions
        { Default, Up, Down, Right, Left }

        public readonly List<Color> PlayerColors = new List<Color>() { Color.FromArgb(255, 255, 100, 100), Color.FromArgb(255, 100, 255, 100), Color.FromArgb(255, 100, 100, 255) };

        public enum CreationStage
        { None, Class, Name }

        private GameCharacter _leftCharacter;
        private GameCharacter _midCharacter;
        private GameCharacter _rightCharacter;

        private CharacterButtonBodyOwner _rightCharacterButton;
        private CharacterButtonBodyOwner _midCharacterButton;
        private CharacterButtonBodyOwner _leftCharacterButton;
        private BasicBodyOwner _playButton;

        private BasicBodyOwner _soundLogo;
        private BasicBodyOwner _dateTime;

        private List<IDynamicGameObject> _dynamicGameObjects = new();

        public static string CharacterTypeToRus(GameCharacterType type)
        {
            var characterType = "";

            switch (type)
            {
                case GameCharacterType.Warrior:
                    characterType = "Боец";
                    break;

                case GameCharacterType.Healer:
                    characterType = "Врачеватель";
                    break;

                case GameCharacterType.Hacker:
                    characterType = "Хакер";
                    break;
            }

            return characterType;
        }

        public static ObjectBody CharacterBodyByType(GameCharacterType type, Color? color = null)
        {
            Color selectedColor;
            if (color.HasValue)
            {
                selectedColor = color.Value;
            }
            else
            {
                selectedColor = GameWorld.CharecterCreation.PlayerColors[(int)GameWorld.CharecterCreation.CurrentButton];
            }

            switch (type)
            {
                case GameCharacterType.Warrior:
                    return new WarriorBody(selectedColor);

                case GameCharacterType.Healer:
                    return new HealerBody(selectedColor);

                case GameCharacterType.Hacker:
                    return new HackerBody(selectedColor);
            }

            throw new Exception("Uknown character type");
        }

        public void RemoveChosenCharacter()
        {
            switch (CurrentButton)
            {
                case ButtonType.CharecterLeft:
                    _leftCharacter = null;
                    break;

                case ButtonType.CharecterMid:
                    _midCharacter = null;
                    break;

                case ButtonType.CharecterRight:
                    _rightCharacter = null;
                    break;
            }
        }

        public void InitializeButtons()
        {
            _leftCharacter = _rightCharacter = _midCharacter = null;

            _leftCharacterButton = new(new List<ObjectBody>() { new CharacterButtonBodyAnimationEmpty(),new CharacterButtonBodyAnimationEmpty2(),
                                                                new CharacterButtonBodyAnimationEmpty3(),new CharacterButtonBodyAnimationEmpty4(),
                                                                new CharacterButtonBodyAnimationEmpty5(),new CharacterButtonBodyAnimationEmpty6(),
                                                                new CharacterButtonBodyAnimationEmpty7(),new CharacterButtonBodyAnimationEmpty8(),
                                                                new CharacterButtonBodyAnimationEmpty9() },
                                                                new List<ObjectBody>() { new CharacterButtonBodyAnimationOccupied(ButtonType.CharecterLeft) },
                                                                this, ButtonType.CharecterLeft);
            _leftCharacterButton.MoveTo(1, 3);
            _dynamicGameObjects.Add(_leftCharacterButton);

            _midCharacterButton = new(new List<ObjectBody>() { new CharacterButtonBodyAnimationEmpty(),new CharacterButtonBodyAnimationEmpty2(),
                                                               new CharacterButtonBodyAnimationEmpty3(),new CharacterButtonBodyAnimationEmpty4(),
                                                               new CharacterButtonBodyAnimationEmpty5(),new CharacterButtonBodyAnimationEmpty6(),
                                                               new CharacterButtonBodyAnimationEmpty7(),new CharacterButtonBodyAnimationEmpty8(),
                                                               new CharacterButtonBodyAnimationEmpty9() },
                                                               new List<ObjectBody>() { new CharacterButtonBodyAnimationOccupied(ButtonType.CharecterMid) },
                                                               this, ButtonType.CharecterMid);
            _midCharacterButton.MoveTo(42, 3);
            _dynamicGameObjects.Add(_midCharacterButton);

            _rightCharacterButton = new(new List<ObjectBody>() {new CharacterButtonBodyAnimationEmpty(),new CharacterButtonBodyAnimationEmpty2(),
                                                                new CharacterButtonBodyAnimationEmpty3(),new CharacterButtonBodyAnimationEmpty4(),
                                                                new CharacterButtonBodyAnimationEmpty5(),new CharacterButtonBodyAnimationEmpty6(),
                                                                new CharacterButtonBodyAnimationEmpty7(),new CharacterButtonBodyAnimationEmpty8(),
                                                                new CharacterButtonBodyAnimationEmpty9() },
                                                                new List<ObjectBody>() { new CharacterButtonBodyAnimationOccupied(ButtonType.CharecterRight) },
                                                                this, ButtonType.CharecterRight);
            _rightCharacterButton.MoveTo(83, 3);
            _dynamicGameObjects.Add(_rightCharacterButton);

            _playButton = new(new List<ObjectBody>() { new CharacterCreationPlayButtonBodyUnselected(), new CharacterCreationPlayButtonSelected() });
            _playButton.MoveTo(42, 26);

            _dateTime = new BasicBodyOwner(new List<ObjectBody>() { new DateTimeBody() });
            _soundLogo = new BasicBodyOwner(new List<ObjectBody>() { new SoundLogoBody() });
            _soundLogo.MoveTo(120, 0);
        }

        public GameCharacter GetGameCharacter(ButtonType type)
        {
            switch (type)
            {
                case ButtonType.CharecterLeft:
                    return LeftCharacter;

                case ButtonType.CharecterMid:
                    return MidCharacter;

                case ButtonType.CharecterRight:
                    return RightCharacter;
            }
            throw new Exception("Uknown button type");
        }

        public GameCharacter LeftCharacter
        {
            get { return _leftCharacter; }
        }

        public GameCharacter MidCharacter
        {
            get { return _midCharacter; }
        }

        public GameCharacter RightCharacter
        {
            get { return _rightCharacter; }
        }

        public List<RepresantationPoint> UILayer = new();

        private ButtonType _currentButton;

        public ButtonType CurrentButton
        {
            get { return _currentButton; }
            private set
            {
                if ((int)value > (int)ButtonType.CharecterRight)
                {
                    value = ButtonType.CharecterLeft;
                }
                else if ((int)value < (int)ButtonType.CharecterLeft)
                {
                    value = ButtonType.CharecterRight;
                }
                _currentButton = value;
            }
        }

        private CreationStage _currentCreationStage;

        public CreationStage CurrentCreationStage
        {
            get { return _currentCreationStage; }
            set
            {
                if ((int)value > (int)CreationStage.Name)
                {
                    value = CreationStage.Name;
                }
                else if ((int)value < 0)
                {
                    value = 0;
                }
                _currentCreationStage = value;
            }
        }

        public bool ReadyToStart
        {
            get
            {
                return LeftCharacter != null || MidCharacter != null || RightCharacter != null;
            }
        }

        public enum SelectedCharacter
        { Left, Mid, Right }

        public SelectedCharacter CurrentCharacter;

        private GameCharacterType _chosenType;

        public GameCharacterType ChosenCharacterType
        {
            get { return _chosenType; }
            set
            {
                if ((int)value >= Enum.GetNames(typeof(GameCharacterType)).Length)
                {
                    value = 0;
                }
                else if ((int)value < 0)
                {
                    value = (GameCharacterType)Enum.GetNames(typeof(GameCharacterType)).Length - 1;
                }
                _chosenType = value;
            }
        }

        //public ObjectBody GetGameCharacterBody()
        //{
        //    switch (ChosenCharacterType)
        //    {
        //        case GameCharacterType.Attacker:
        //            {
        //                return new WarriorBody();
        //            }
        //
        //
        //        case GameCharacterType.Healer:
        //            {
        //                return new WarriorBody();
        //            }
        //
        //
        //        default:
        //            throw new Exception("[ERROR] Uknown character type");
        //    }
        //}

        public bool IsCurrentButtonReady()
        {
            IDynamicGameObject button;

            switch (CurrentButton)
            {
                case ButtonType.CharecterLeft:

                    button = (IDynamicGameObject)_leftCharacterButton;
                    return button.IsBusy();

                case ButtonType.CharecterMid:
                    button = (IDynamicGameObject)_midCharacterButton;
                    return button.IsBusy();

                case ButtonType.CharecterRight:
                    button = (IDynamicGameObject)_leftCharacterButton;
                    return button.IsBusy();
            }
            return true;
        }

        public void CreateGameCharacter()
        {
            var selectedColor = PlayerColors[(int)CurrentButton];

            switch (CurrentCharacter)
            {
                case SelectedCharacter.Left:
                    {
                        _leftCharacter = new GameCharacter(CharacterName, ChosenCharacterType, selectedColor);
                    }
                    break;

                case SelectedCharacter.Mid:
                    {
                        _midCharacter = new GameCharacter(CharacterName, ChosenCharacterType, selectedColor);
                    }
                    break;

                case SelectedCharacter.Right:
                    {
                        _rightCharacter = new GameCharacter(CharacterName, ChosenCharacterType, selectedColor);
                    }
                    break;
            }
        }

        public bool EmptyName
        {
            get { return CharacterName == ""; }
        }

        public void ResetNameValuable()
        {
            CharacterName = "";
        }

        public void RemoveLastCharFromName()
        {
            if (CharacterName.Length > 0)
            {
                CharacterName = CharacterName.Remove(CharacterName.Length - 1);
            }
        }

        private string _characterName = "";

        public string CharacterName
        {
            get { return _characterName; }
            set
            {
                if (value.Length > 10)
                {
                    value = value.Substring(0, 10);
                }
                _characterName = value;
            }
        }

        private void HighlightButton()
        {
            _playButton.State = 0;

            switch (_currentButton)
            {
                case ButtonType.Play:
                    {
                        _playButton.State = 1;
                    }
                    break;
            }
        }


        public void ChangeButton(ButtonInteractions interactions)
        {

            if (_currentButton != ButtonType.Play)
            {
                switch (interactions)
                {
                    case ButtonInteractions.Default:
                        {
                            _currentButton = ButtonType.CharecterLeft;
                        }
                        break;

                    case ButtonInteractions.Down:
                        {
                            _currentButton = ButtonType.Play;
                        }
                        break;

                    case ButtonInteractions.Right:
                        {
                            CurrentButton++;
                        }
                        break;

                    case ButtonInteractions.Left:
                        {
                            CurrentButton--;
                        }
                        break;
                }
            }
            else
            {
                switch (interactions)
                {
                    case ButtonInteractions.Up:
                        {
                            _currentButton = ButtonType.CharecterMid;
                        }
                        break;
                }
            }

            HighlightButton();
        }

        public List<List<IRenderableObject>> GetStaticObjects()
        {
            Update();

            var returnList = new List<List<IRenderableObject>>();

            var buttons = new List<IRenderableObject>()
            {
                _leftCharacterButton, _midCharacterButton, _rightCharacterButton, _playButton,
                _soundLogo, _dateTime
            };

            returnList.Add(buttons);
            return returnList;
        }

        List<List<IRenderableObject>> IGameScene.GetGameObjects()
        {
            Update();

            return new List<List<IRenderableObject>>();
        }

        public void Update()
        {
            foreach (var gameObject in _dynamicGameObjects)
            {
                gameObject.Update();
            }
        }
    }
}