﻿using System.Drawing;
using TheGallowsSurvival.GameScenes;

namespace TheGallowsSurvival.GameScene
{
    public enum GameCharacterType
    { Warrior, Healer, Hacker }

    internal static class GameWorld
    {
        public static ScreenStats Screen = new();

        public readonly static List<string> CharacterNames = new()
        {
            "Абрахам",
            "Альбус",
            "Алекс",
            "Блейд",
            "Бертон",
            "Эмброуз",
            "Артур",
            "Боуи",
            "Картер",
            "Коннор",
            "Дэви",
            "Декард",
            "Донован",
            "Дракс",
            "Эммет",
            "Харрисон",
            "Джек",
            "Джексон",
            "Джейми",
            "Джарет",
            "Джейсон",
            "Джон",
            "Кирк",
            "Леонард",
            "Лекс",
            "Люк",
            "Мейс",
            "Малкольм",
            "Монтгомери",
            "Малдер",
            "Мерфи",
            "Николас",
            "Патрик",
            "Питер",
                        "Рэй",
            "Сэм",
                        "Серлинг",
            "Тернер",
                        "Тирион",
            "Верн",
                        "Виктор",
        };

        // Game scenes
        public static MainMenuScene MainMenu = new();

        public static CharacterCreationScene CharecterCreation = new();
        public static GlobalMapScene GlobalMap = new();

        // Current game scene
        public static IGameScene CurrentGameScene = MainMenu;

        public static bool IsSoundEnabled = true;
        public static bool GameStarted = false;

        public static GameDifficulty? SelectedDifficulty;
        public static DateTime GameStartTime = DateTime.Now;
        public static DateTime GameEndTime = DateTime.Now;

        public static readonly int WORLD_WIDTH = 123;
        public static readonly int WORLD_HEIGHT = 33;

        public static int EnergyLeft;
        public static int MaxEnergy = 0;

    }

    internal interface IDiableCharacter
    {
        public string Name { get; }
        public int HP { get; }
        public int MaxHP { get; }
        public Color CharacterColor { get; }

        public bool TakeDamage(int damage);
    }

    internal class GameCharacter : IDiableCharacter
    {
        public GameCharacter(string name, GameCharacterType type, Color playerColor)
        {
            _name = name;
            _characterColor = playerColor;

            Type = type;

            switch (Type)
            {
                case GameCharacterType.Warrior:
                    _maxHP = 20;
                    LeftAction = new BattleAction("Колющий удар", "Базовый удар не требующий энергии для использования\nНеплохой урон по одной цели", 0, 3);
                    RightAction = new BattleAction("Удар с размаха", "Усиленный удар, трубуется энергия для исполнения\nБольшой урон по одной цели", 15, 5);
                    break;

                case GameCharacterType.Healer:
                    LeftAction = new BattleAction("Удар тростью", "Крайне не эффективный удар, который однако может добить врага\nНе требует энергии", 0, 1);
                    RightAction = new BattleAction("Иссциление", "Вместо нанесения урона, исцеляет выбранного героя\nИссцеления одной цели", 15, -5);
                    _maxHP = 15;
                    break;

                case GameCharacterType.Hacker:
                    LeftAction = new BattleAction("Перехват", "Слабый удар, полезен только для добивания врагов\n Слабый урон по одной цели, не требует энергии", 0, 2);
                    RightAction = new BattleAction("Взлом", "Не наносит урон, однако восстанавливает энергию команды\nВосстановление энергии", -20, 0);
                    _maxHP = 15;
                    break;
            }

            _HP = _maxHP;
        }

        public bool TakeDamage(int damage)
        {
            _HP -= damage;

            if (_HP > _maxHP)
            {
                _HP = _maxHP;
            }

            return (_HP <= 0);
        }

        private Color _characterColor;
        public GameCharacterType Type;
        private string _name;
        private int _maxHP;
        private int _HP;

        public BattleAction LeftAction;
        public BattleAction RightAction;

        public string Name
        { get { return _name; } }

        public int HP
        { get { return _HP; } }

        public int MaxHP
        { get { return _maxHP; } }

        public Color CharacterColor
        { get { return _characterColor; } }
    }

    internal class ScreenStats
    {
        // W:123 H:33
        public readonly int WIDTH = 123;

        public readonly int HEIGHT = 33;
        public readonly Point OFFSET = new(17, 2);
        public Point CameraPoint = new(17, 2);
        public readonly int CAMERA_WIDTH = 89;
        public readonly int CAMERA_HEIGHT = 23;
    }
}