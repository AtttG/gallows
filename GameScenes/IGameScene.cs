﻿using TheGallowsSurvival.GameObjects;

namespace TheGallowsSurvival.GameScene
{
    internal interface IGameScene
    {
        // Объекты с позицией привязаной к экрану
        public List<List<IRenderableObject>> GetStaticObjects();

        // Объекты с позицией привязаной к игровому миру
        public List<List<IRenderableObject>> GetGameObjects();

        public bool IsBusy { get; }

        public void Update();
    }
}