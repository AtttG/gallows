﻿namespace TheGallowsSurvival
{
    internal class BattleAction
    {
        public BattleAction(string name, string description, int cost, int damage)
        {
            Name = name;
            Description = description;
            Cost = cost;
            Damage = damage;
        }

        public string Name;
        public string Description;
        public int Cost;
        public int Damage;
    }
}