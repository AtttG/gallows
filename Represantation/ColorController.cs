﻿using System.Drawing;

namespace TheGallowsSurvival.Represantation
{
    internal static class ColorController
    {
        public static Color GetRandomColor(int lowerLimit = 0)
        {
            var random = new Random();

            int r = random.Next(lowerLimit, 256);
            int g = random.Next(lowerLimit, 256);
            int b = random.Next(lowerLimit, 256);

            return Color.FromArgb(255, r, g, b);
        }

        public static Color GetShimmerColor(Color baseColor)
        {
            var currentTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            int dif = (int)((currentTime) % (100 * 2)) / 2;

            var r = (baseColor.R + dif > 255) ? (255 - ((baseColor.R + dif) - 255)) : baseColor.R + dif;
            var g = (baseColor.G + dif > 255) ? (255 - ((baseColor.G + dif) - 255)) : baseColor.G + dif;
            var b = (baseColor.B + dif > 255) ? (255 - ((baseColor.B + dif) - 255)) : baseColor.B + dif;

            return Color.FromArgb(baseColor.A, r, g, b);
        }

        public static Color GetRgbColor()
        {
            var currentTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            int r = (int)((currentTime + 170 * 5) % (510 * 5)) / 5;
            int g = (int)((currentTime + 340 * 5) % (510 * 5)) / 5;
            int b = (int)((currentTime) % (510 * 5)) / 5;

            if (r > 255) { r = 255 - (r - 255); }
            if (g > 255) { g = 255 - (g - 255); }
            if (b > 255) { b = 255 - (b - 255); }

            return Color.FromArgb(255, r, g, b);
        }

        public static Color GetDarkRgbColor()
        {
            var currentTime = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();

            int r = (int)((currentTime + 170 * 5) % (510 * 5)) / 5;
            int g = (int)((currentTime + 340 * 5) % (510 * 5)) / 5;
            int b = (int)((currentTime) % (510 * 5)) / 5;

            if (r > 255) { r = 255 - (r - 255); }
            if (g > 255) { g = 255 - (g - 255); }
            if (b > 255) { b = 255 - (b - 255); }

            return Color.FromArgb(255, (int)Math.Round(r * 0.5), (int)Math.Round(g * 0.5), (int)Math.Round(b * 0.5));
        }
    }
}