﻿using System.Drawing;

namespace TheGallowsSurvival.Represantation
{
    internal class RepresantationPoint
    {
        public RepresantationPoint(RepresantationPoint point)
        {
            Position = point.Position;
            Pixel = point.Pixel;
        }

        public RepresantationPoint(Point cords, Pixel pixel)
        {
            Position = cords;
            Pixel = pixel;
        }

        public void Move(int x, int y)
        {
            Position = new Point(Position.X + x, Position.Y + y);
        }

        public Point Position;
        public Pixel Pixel;
    }
}