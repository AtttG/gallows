﻿using System.Drawing;

namespace TheGallowsSurvival.Represantation
{
    internal class Pixel
    {
        public void SetSymbol(char symbol)
        {
            _value = symbol.ToString();
        }

        private string _value;

        public string Value
        {
            get { return _value; }
        }

        private Color _foreground;

        public Color Foreground
        {
            set { _foreground = value; }
            get { return _foreground; }
        }

        public Pixel(char? symbol = null, Color? foreground = null)
        {
            _value = symbol.HasValue ? symbol.Value.ToString() : " ";
            _foreground = foreground.HasValue ? foreground.Value : Color.White;
        }
    }
}