﻿using System.Drawing;
using TheGallowsSurvival.GameScene;
using TheGallowsSurvival.GameScenes;

namespace TheGallowsSurvival
{
    internal class GameSceneController
    {
        private enum GameState
        { MainMenu, CharacterCreation, GlobalMap }

        private GameSceneController()
        {
        }

        private static GameSceneController _instance;

        public static GameSceneController GetInstance()
        {
            if (_instance == null)
            {
                _instance = new GameSceneController();
                _instance._state = GameState.MainMenu;
                GameWorld.MainMenu.InitializeButtons();
            }
            return _instance;
        }

        private GameState _state;

        public void StateCharacterCreation()
        {
            switch (_state)
            {
                case GameState.MainMenu:
                    {
                        GameWorld.SelectedDifficulty = GameWorld.MainMenu.SelectedDifficulty;
                    }
                    break;
            }

            SetState(GameState.CharacterCreation);
        }

        public void StateMainMenu()
        {

            SetState(GameState.MainMenu);
        }

        public void StateGlobalMap()
        {
            switch (_state)
            {
                case GameState.MainMenu:

                    break;

                case GameState.CharacterCreation:

                    break;

            }

            SetState(GameState.GlobalMap);
        }

        private void SetState(GameState state)
        {
            switch (state)
            {
                case GameState.MainMenu:
                    {
                        GameWorld.MainMenu.InitializeButtons();
                        GameWorld.MainMenu.ChangeButton(MainMenuScene.ButtonInteractions.Default);
                        GameWorld.CurrentGameScene = GameWorld.MainMenu;
                    }
                    break;

                case GameState.CharacterCreation:
                    {
                        GameWorld.CharecterCreation.InitializeButtons();
                        GameWorld.CharecterCreation.ChangeButton(CharacterCreationScene.ButtonInteractions.Default);
                        GameWorld.CurrentGameScene = GameWorld.CharecterCreation;
                    }
                    break;

                case GameState.GlobalMap:
                    {
                        GameWorld.GlobalMap.InitializeScene();
                        GameWorld.CurrentGameScene = GameWorld.GlobalMap;

                        GameWorld.GameStartTime = DateTime.Now;

                        if (!GameWorld.GameStarted)
                        {
                            switch (GameWorld.SelectedDifficulty)
                            {
                                case GameDifficulty.Easy:
                                    GameWorld.EnergyLeft = GameWorld.MaxEnergy = 70;
                                    break;

                                case GameDifficulty.Medium:
                                    GameWorld.EnergyLeft = GameWorld.MaxEnergy = 60;
                                    break;

                                case GameDifficulty.Hard:
                                    GameWorld.EnergyLeft = GameWorld.MaxEnergy = 50;
                                    break;
                            }
                        }

                        GameWorld.GameStarted = true;
                    }
                    break;
            }

            _state = state;
        }

        public void HandleInput(ConsoleKeyInfo keyInfo)
        {
            //GameWorld.CurrentGameScene.Update();

            switch (_state)
            {
                case (GameState.MainMenu):
                    {
                        if (!GameWorld.MainMenu.IsBusy)
                        {
                            switch (keyInfo.Key)
                            {
                                case (ConsoleKey.Enter):
                                    {
                                        if (GameWorld.MainMenu.SelectingDifficulty)
                                        {
                                            // Переход на стадию создания персонажа
                                            GameSoundPlayer.PlayButtonClickSound();
                                            StateCharacterCreation();
                                        }
                                        else
                                        {
                                            switch (GameWorld.MainMenu.CurrentButtonType)
                                            {
                                                case MainMenuScene.ButtonType.Play:
                                                    {
                                                        GameSoundPlayer.PlayButtonClickSound();
                                                        if (GameWorld.GameStarted)
                                                        {
                                                            // Продолжение игры
                                                            StateGlobalMap();
                                                        }
                                                        else
                                                        {
                                                            // Начало процесса выбора сложности
                                                            GameWorld.MainMenu.SwitchDifficultySelection(true);
                                                        }
                                                    }
                                                    break;

                                                case MainMenuScene.ButtonType.Sound:
                                                    {
                                                        GameSoundPlayer.PlayButtonClickSound();
                                                        GameWorld.IsSoundEnabled = !GameWorld.IsSoundEnabled;
                                                    }
                                                    break;

                                                case MainMenuScene.ButtonType.Exit:
                                                    {
                                                        
                                                        if (GameWorld.MainMenu.EnsuringExit)
                                                        {
                                                            Environment.Exit(0);
                                                        }
                                                        else
                                                        {
                                                            GameSoundPlayer.PlayButtonClickSound();
                                                            GameWorld.MainMenu.SwitchEnsuringExit(true);
                                                        }
                                                    }
                                                    break;
                                            }
                                        }
                                    }
                                    break;

                                case (ConsoleKey.UpArrow):
                                    {
                                        if (!GameWorld.MainMenu.SelectingDifficulty && !GameWorld.MainMenu.EnsuringExit)
                                        {
                                            GameSoundPlayer.PlayButtonSound();
                                            GameWorld.MainMenu.ChangeButton(MainMenuScene.ButtonInteractions.Up);
                                        }
                                    }
                                    break;

                                case (ConsoleKey.DownArrow):
                                    {
                                        if (!GameWorld.MainMenu.SelectingDifficulty && !GameWorld.MainMenu.EnsuringExit)
                                        {
                                            GameSoundPlayer.PlayButtonSound();
                                            GameWorld.MainMenu.ChangeButton(MainMenuScene.ButtonInteractions.Down);
                                        }
                                    }
                                    break;

                                case (ConsoleKey.RightArrow):
                                    {
                                        if (GameWorld.MainMenu.SelectingDifficulty && !GameWorld.MainMenu.EnsuringExit)
                                        {
                                            GameSoundPlayer.PlayButtonSound();
                                            GameWorld.MainMenu.ChangeButton(MainMenuScene.ButtonInteractions.Down);
                                        }
                                    }
                                    break;

                                case (ConsoleKey.LeftArrow):
                                    {
                                        if (GameWorld.MainMenu.SelectingDifficulty && !GameWorld.MainMenu.EnsuringExit)
                                        {
                                            GameSoundPlayer.PlayButtonSound();
                                            GameWorld.MainMenu.ChangeButton(MainMenuScene.ButtonInteractions.Up);
                                        }
                                    }
                                    break;

                                case (ConsoleKey.Escape):
                                    {
                                        if (GameWorld.MainMenu.SelectingDifficulty)
                                        {
                                            GameSoundPlayer.PlayButtonClickSound();
                                            GameWorld.MainMenu.SwitchDifficultySelection(false);
                                        }

                                        if (GameWorld.MainMenu.CurrentButtonType == MainMenuScene.ButtonType.Exit && GameWorld.MainMenu.EnsuringExit)
                                        {
                                            GameSoundPlayer.PlayButtonClickSound();
                                            GameWorld.MainMenu.SwitchEnsuringExit(false);
                                        }
                                    }
                                    break;
                            }
                        }
                        break;
                    }

                case (GameState.CharacterCreation):
                    {
                        if (!GameWorld.CharecterCreation.IsBusy)
                        {
                            switch (GameWorld.CharecterCreation.CurrentCreationStage)
                            {
                                // Не находится в процессе создания персонажа
                                case (CharacterCreationScene.CreationStage.None):
                                    {
                                        switch (keyInfo.Key)
                                        {
                                            case (ConsoleKey.Delete):
                                                {
                                                    GameSoundPlayer.PlayDamageSound();
                                                    GameWorld.CharecterCreation.RemoveChosenCharacter();
                                                }
                                                break;

                                            case (ConsoleKey.Escape):
                                                {
                                                    GameSoundPlayer.PlayButtonSound();
                                                    StateMainMenu();
                                                }
                                                break;

                                            case (ConsoleKey.Enter):
                                                {
                                                    switch (GameWorld.CharecterCreation.CurrentButton)
                                                    {
                                                        case CharacterCreationScene.ButtonType.Play:
                                                            {
                                                                if (GameWorld.CharecterCreation.ReadyToStart)
                                                                {
                                                                    GameSoundPlayer.PlayButtonClickSound();
                                                                    StateGlobalMap();
                                                                }
                                                            }
                                                            break;

                                                        case CharacterCreationScene.ButtonType.CharecterLeft:
                                                            {
                                                                // Начало создания левого персонажа
                                                                GameSoundPlayer.PlayButtonClickSound();
                                                                GameWorld.CharecterCreation.CurrentCreationStage++;
                                                                GameWorld.CharecterCreation.CurrentCharacter = CharacterCreationScene.SelectedCharacter.Left;
                                                            }
                                                            break;

                                                        case CharacterCreationScene.ButtonType.CharecterMid:
                                                            {
                                                                // Начало создания среднего персонажа
                                                                GameSoundPlayer.PlayButtonClickSound();
                                                                GameWorld.CharecterCreation.CurrentCreationStage++;
                                                                GameWorld.CharecterCreation.CurrentCharacter = CharacterCreationScene.SelectedCharacter.Mid;
                                                            }
                                                            break;

                                                        case CharacterCreationScene.ButtonType.CharecterRight:
                                                            {
                                                                // Начало создания правого персонажа
                                                                GameSoundPlayer.PlayButtonClickSound();
                                                                GameWorld.CharecterCreation.CurrentCreationStage++;
                                                                GameWorld.CharecterCreation.CurrentCharacter = CharacterCreationScene.SelectedCharacter.Right;
                                                            }
                                                            break;
                                                    }
                                                }
                                                break;

                                            case (ConsoleKey.UpArrow):
                                                {
                                                    GameSoundPlayer.PlayButtonSound();
                                                    GameWorld.CharecterCreation.ChangeButton(CharacterCreationScene.ButtonInteractions.Up);
                                                }
                                                break;

                                            case (ConsoleKey.DownArrow):
                                                {
                                                    GameSoundPlayer.PlayButtonSound();
                                                    GameWorld.CharecterCreation.ChangeButton(CharacterCreationScene.ButtonInteractions.Down);
                                                }
                                                break;

                                            case (ConsoleKey.RightArrow):
                                                {
                                                    GameSoundPlayer.PlayButtonSound();
                                                    GameWorld.CharecterCreation.ChangeButton(CharacterCreationScene.ButtonInteractions.Right);
                                                }
                                                break;

                                            case (ConsoleKey.LeftArrow):
                                                {
                                                    GameSoundPlayer.PlayButtonSound();
                                                    GameWorld.CharecterCreation.ChangeButton(CharacterCreationScene.ButtonInteractions.Left);
                                                }
                                                break;
                                        }
                                    }
                                    break;
                                // Стадия выбора класса персонажа
                                case CharacterCreationScene.CreationStage.Class:
                                    {
                                        switch (keyInfo.Key)
                                        {
                                            case (ConsoleKey.Escape):
                                                {
                                                    GameSoundPlayer.PlayButtonClickSound();
                                                    GameWorld.CharecterCreation.CurrentCreationStage--;
                                                }
                                                break;

                                            case (ConsoleKey.Enter):
                                                {
                                                    GameSoundPlayer.PlayButtonClickSound();
                                                    GameWorld.CharecterCreation.CurrentCreationStage++;
                                                    if (GameWorld.CharecterCreation.CharacterName == "")
                                                    {
                                                        var random = new Random();

                                                        var nameIndex = random.Next(0, GameWorld.CharacterNames.Count);

                                                        GameWorld.CharecterCreation.CharacterName = GameWorld.CharacterNames[nameIndex];
                                                    }

                                                }
                                                break;

                                            case (ConsoleKey.RightArrow):
                                                {
                                                    GameSoundPlayer.PlayButtonSound();
                                                    GameWorld.CharecterCreation.ChosenCharacterType++;
                                                }
                                                break;

                                            case (ConsoleKey.LeftArrow):
                                                {
                                                    GameSoundPlayer.PlayButtonSound();
                                                    GameWorld.CharecterCreation.ChosenCharacterType--;
                                                }
                                                break;
                                        }
                                    }
                                    break;
                                // Стадия выбора имени персонажа
                                case CharacterCreationScene.CreationStage.Name:
                                    {
                                        switch (keyInfo.Key)
                                        {
                                            case (ConsoleKey.Escape):
                                                {
                                                    // Откат на выбор класса
                                                    GameSoundPlayer.PlayButtonClickSound();
                                                    GameWorld.CharecterCreation.ResetNameValuable();
                                                    GameWorld.CharecterCreation.CurrentCreationStage--;
                                                }
                                                break;

                                            case (ConsoleKey.Enter):
                                                {
                                                    // Если имя соответствует требованиям:

                                                    if (!GameWorld.CharecterCreation.EmptyName)
                                                    {
                                                        GameSoundPlayer.PlayButtonClickSound();
                                                        GameWorld.CharecterCreation.CreateGameCharacter();
                                                        GameWorld.CharecterCreation.CurrentCreationStage = CharacterCreationScene.CreationStage.None;
                                                        GameWorld.CharecterCreation.ResetNameValuable();
                                                    }
                                                }
                                                break;

                                            default:
                                                {
                                                    if (keyInfo.Key == ConsoleKey.Backspace)
                                                    {
                                                        GameSoundPlayer.PlayButtonClickSound();
                                                        GameWorld.CharecterCreation.RemoveLastCharFromName();
                                                    }
                                                    else if (!(keyInfo.Key == ConsoleKey.DownArrow ||
                                                        keyInfo.Key == ConsoleKey.RightArrow ||
                                                        keyInfo.Key == ConsoleKey.UpArrow ||
                                                        keyInfo.Key == ConsoleKey.LeftArrow))
                                                    {
                                                        GameSoundPlayer.PlayButtonClickSound();
                                                        GameWorld.CharecterCreation.CharacterName += keyInfo.KeyChar;
                                                    }
                                                }

                                                break;
                                        }
                                    }
                                    break;
                            }
                        }
                        break;
                    }

                case (GameState.GlobalMap):
                    {
                        if (!GameWorld.GlobalMap.IsBusy)
                        {
                            switch (GameWorld.GlobalMap.State)
                            {
                                case GlobalMapScene.SubState.MOVING:
                                    {
                                        switch (keyInfo.Key)
                                        {
                                            case (ConsoleKey.Escape):
                                                {
                                                    StateMainMenu();
                                                }
                                                break;

                                            case (ConsoleKey.RightArrow):
                                                {
                                                    var isMoved = GameWorld.GlobalMap.MovePlayer(new Point(1, 0));
                                                    if (isMoved)
                                                    {
                                                        GameWorld.Screen.CameraPoint = new Point
                                                        (
                                                            GameWorld.Screen.CameraPoint.X + 1,
                                                            GameWorld.Screen.CameraPoint.Y
                                                        );
                                                    }
                                                }
                                                break;

                                            case (ConsoleKey.LeftArrow):
                                                {
                                                    var isMoved = GameWorld.GlobalMap.MovePlayer(new Point(-1, 0));
                                                    if (isMoved)
                                                    {
                                                        GameWorld.Screen.CameraPoint = new Point
                                                        (
                                                            GameWorld.Screen.CameraPoint.X - 1,
                                                            GameWorld.Screen.CameraPoint.Y
                                                        );
                                                    }
                                                }
                                                break;

                                            case (ConsoleKey.DownArrow):
                                                {
                                                    var isMoved = GameWorld.GlobalMap.MovePlayer(new Point(0, 1));
                                                    if (isMoved)
                                                    {
                                                        GameWorld.Screen.CameraPoint = new Point
                                                        (
                                                            GameWorld.Screen.CameraPoint.X,
                                                            GameWorld.Screen.CameraPoint.Y + 1
                                                        );
                                                    }
                                                }
                                                break;

                                            case (ConsoleKey.UpArrow):
                                                {
                                                    var isMoved = GameWorld.GlobalMap.MovePlayer(new Point(0, -1));
                                                    if (isMoved)
                                                    {
                                                        GameWorld.Screen.CameraPoint = new Point
                                                        (
                                                            GameWorld.Screen.CameraPoint.X,
                                                            GameWorld.Screen.CameraPoint.Y - 1
                                                        );
                                                    }
                                                }
                                                break;
                                        }
                                    }
                                    break;

                                case GlobalMapScene.SubState.BATTLE:
                                    {
                                        if (GameWorld.GlobalMap.IsPlayersMove)
                                        {
                                            if (!GameWorld.GlobalMap.IsExecutingAction)
                                            {
                                                switch (keyInfo.Key)
                                                {
                                                    case (ConsoleKey.DownArrow):
                                                        {
                                                            GameSoundPlayer.PlayButtonSound();
                                                            GameWorld.GlobalMap.SwitchSelectedCharacter(1);
                                                        }
                                                        break;

                                                    case (ConsoleKey.UpArrow):
                                                        {
                                                            GameSoundPlayer.PlayButtonSound();
                                                            GameWorld.GlobalMap.SwitchSelectedCharacter(-1);
                                                        }
                                                        break;

                                                    case (ConsoleKey.RightArrow):
                                                        {
                                                            GameWorld.GlobalMap.SwitchSelectedActions();
                                                        }
                                                        break;

                                                    case (ConsoleKey.LeftArrow):
                                                        {
                                                            GameWorld.GlobalMap.SwitchSelectedActions();
                                                        }
                                                        break;

                                                    case (ConsoleKey.Enter):
                                                        {
                                                            // Переключится на исполнение действия
                                                            GameSoundPlayer.PlayButtonClickSound();
                                                            GameWorld.GlobalMap.SwitchActionExecution(true);
                                                        }
                                                        break;
                                                }
                                            }
                                            else
                                            {
                                                switch (keyInfo.Key)
                                                {
                                                    case (ConsoleKey.DownArrow):
                                                        {
                                                            GameSoundPlayer.PlayButtonSound();
                                                            GameWorld.GlobalMap.SwitchExecutionTarget(GlobalMapScene.TargetSelectionSwitch.DOWN);
                                                        }
                                                        break;

                                                    case (ConsoleKey.UpArrow):
                                                        {
                                                            GameSoundPlayer.PlayButtonSound();
                                                            GameWorld.GlobalMap.SwitchExecutionTarget(GlobalMapScene.TargetSelectionSwitch.UP);
                                                        }
                                                        break;

                                                    case (ConsoleKey.RightArrow):
                                                        {
                                                            GameSoundPlayer.PlayButtonSound();
                                                            GameWorld.GlobalMap.SwitchExecutionTarget(GlobalMapScene.TargetSelectionSwitch.RIGHT);
                                                        }
                                                        break;

                                                    case (ConsoleKey.LeftArrow):
                                                        {
                                                            GameSoundPlayer.PlayButtonSound();
                                                            GameWorld.GlobalMap.SwitchExecutionTarget(GlobalMapScene.TargetSelectionSwitch.LEFT);
                                                        }
                                                        break;

                                                    case (ConsoleKey.Escape):
                                                        {
                                                            GameSoundPlayer.PlayButtonClickSound();
                                                            GameWorld.GlobalMap.SwitchActionExecution(false);
                                                        }
                                                        break;

                                                    case (ConsoleKey.Enter):
                                                        {
                                                            GameSoundPlayer.PlayButtonClickSound();
                                                            GameWorld.GlobalMap.ExecuteAction();
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                    break;

                                case GlobalMapScene.SubState.END_SCREEN:
                                    {
                                        Environment.Exit(0);
                                    }
                                    break;
                            }
                        }
                        break;
                    }
            }
        }
    }
}