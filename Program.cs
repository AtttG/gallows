﻿using TheGallowsSurvival;
using TheGallowsSurvival._Console;
using TheGallowsSurvival.GameScene;

var input = GameSceneController.GetInstance();
var frameMaker = new FrameMaker();
var screen = new ScreenUpdater(frameMaker);
GameSoundPlayer.LoadSounds();

while (true)
{
    GameWorld.CurrentGameScene.Update();

    try
    {
        var response = ConsoleReader.ReadLine(1);

        input.HandleInput(response);
    }
    catch (TimeoutException) { }

    screen.ShowFrame();
}
